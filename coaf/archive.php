<?php get_header(); ?>

<?php the_archive_title(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'content', 'archive' ); ?>
<?php endwhile; endif; ?>

<?php get_footer(); ?>