<?php
/**
 * Render the logo
 */

$custom_logo      = vinchflowers_option( 'custom_logo', false, 'url' );
$mobile_logo      = vinchflowers_option( 'mobile_logo', false, 'url' );
$light_logo      = vinchflowers_option( 'transparent_logo', false, 'url' );

$transparent = get_post_meta(get_the_ID(), 'page_header_display', true );
if($transparent == true){ ?>
 
<?php if( !empty( $light_logo ) ) { ?>
    <img src="<?php echo esc_url( $light_logo ) ?>" class="has-retina light-logo jch-lazyloaded" alt="<?php bloginfo( 'Name' ) ?>">
    <img src="<?php echo esc_url( $custom_logo ) ?>" class="has-retina dark-logo jch-lazyloaded" alt="<?php bloginfo( 'Name' ) ?>">
<?php } else { ?>
    <img src="<?php echo esc_url( $custom_logo ) ?>" class="has-retina dark-logo" alt="<?php bloginfo( 'Name' ) ?>">
<?php } ?>

<?php }else{ ?>
   <?php if( !empty( $custom_logo ) ) { ?>
    <img src="<?php echo esc_url( $custom_logo ) ?>" class="has-retina dark-logo" alt="<?php bloginfo( 'Name' ) ?>">
<?php } else { ?>
    <span class="site-title"><?php bloginfo( 'Name' ) ?></span>
<?php } ?>
<?php } ?>





