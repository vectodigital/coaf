<?php global $coaf_options; ?>
<div class="footer-bottom__copyright-text">
<?php echo wp_kses_post( $coaf_options['copyright_text'] ) ?>

</div>
<?php echo wp_kses_post( $coaf_options['creater_text'] ); ?>
<a class="footer-bottom__vecto" href="https://vecto.digital" target="_blank"><object class="" type="image/svg+xml" data="<?php echo get_template_directory_uri(); ?>/assets/imgs/vecto.svg"></object></a>