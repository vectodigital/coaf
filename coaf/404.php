<?php get_header(); ?>

<?php echo __( '404 Not Found', 'coaf' ); ?>
<p><?php echo __( 'The page you requested could not be found.', 'coaf' ); ?></p>

<?php get_footer(); ?>