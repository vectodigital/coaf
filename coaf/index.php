<?php get_header(); ?>
test
<main class="main" id="main">
    <h1 class="main__title">Children Of Armenia Fund</h1>
    <div class="main__hero-wrapper">
      <div class="main__hero">
        <div class="main__hero-item">
          <img class="main__hero-img" src="<?php echo get_template_directory_uri(); ?>/imgs/hero.jpg" alt="we help">
          <div class="main__hero-content container">
            <h2 class="main__hero-title title">We Help</h2>
            <p class="main__hero-descr">Lorem Ipsum is simply dummy text of  the printing and typesetting industry. </p>
          </div>
        </div>
        <div class="main__hero-item">
          <img class="main__hero-img" src="<?php echo get_template_directory_uri(); ?>/imgs/hero.jpg" alt="we help">
          <div class="main__hero-content container">
            <h2 class="main__hero-title title">Renovations</h2>
            <p class="main__hero-descr">Lorem Ipsum is simply dummy text of  the printing and typesetting industry. </p>
          </div>
        </div>
        <div class="main__hero-item">
          <img class="main__hero-img" src="<?php echo get_template_directory_uri(); ?>/imgs/hero.jpg" alt="we help">
          <div class="main__hero-content container">
            <h2 class="main__hero-title title">COAF Smart</h2>
            <p class="main__hero-descr">Lorem Ipsum is simply dummy text of  the printing and typesetting industry. </p>
          </div>
        </div>
        <div class="main__hero-item">
          <img class="main__hero-img" src="<?php echo get_template_directory_uri(); ?>/imgs/hero.jpg" alt="we help">
          <div class="main__hero-content container">
            <h2 class="main__hero-title title">Youth</h2>
            <p class="main__hero-descr">Lorem Ipsum is simply dummy text of  the printing and typesetting industry. </p>
          </div>
        </div>
      </div>
      <div class="main__hero-names container">
        <p class="main__hero-name">We Help</p>
        <p class="main__hero-name">Renovations</p>
        <p class="main__hero-name">COAF Smart</p>
        <p class="main__hero-name">Youth</p>
      </div>
    </div>
    <section class="main__our container">
      <h2 class="main__our-mobtitle title">Our Mission and vision</h2>
      <div class="main__our-text">
        <section class="main__mission">
          <h2 class="main__mission-title title">Our Mission</h2>
          <p class="main__mission-text">All children of Armenia achieve their full potential and contribute to the advancement of the world around them.</p>
        </section>
        <section class="main__vision">
          <h2 class="main__vision-title title">Our Vision</h2>
          <p class="main__vision-text">Provide resources to children and adults with COAF SMART initiatives to advance rural communities through innovation.</p>
        </section>
      </div>
    </section>
    <section class="main__what">
      <img class="main__what-pic" src="<?php echo get_template_directory_uri(); ?>/imgs/what-we-did.jpg" alt="what we did">
      <div class="main__what-text container">
        <div class="main__what-textin">
          <h2 class="main__what-title title">What we did</h2>
          <p class="main__what-descr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s. Lorem Ipsum is simply dummy text of the printing and typesetting industry and Lorem as dummy text.</p>
          <ul class="main__what-list">
            <li class="main__what-item beneficiaries">
              <div class="main__what-info">
                <p class="main__what-num main-black">107,000+</p>
                <p class="main__what-inftext">Beneficiaries</p>
              </div>
            </li>
            <li class="main__what-item programs">
              <div class="main__what-info">
                <p class="main__what-num main-black">35</p>
                <p class="main__what-inftext">Programs</p>
              </div>
            </li>
            <li class="main__what-item renovations">
              <div class="main__what-info">
                <p class="main__what-num main-black">170</p>
                <p class="main__what-inftext">Renovations</p>
              </div>
            </li>          
            <li class="main__what-item communities">
              <div class="main__what-info">
                <p class="main__what-num main-black">64</p>
                <p class="main__what-inftext">Communities</p>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </section>
    <section class="main__donate">
      <div class="main__donate-inner container">
        <div class = "main__donate-heading">
          <h2 class="main__donate-title title">Donate Now</h2>
          <p class="main__donate-subtext main-black">Make your impact today.</p>
        </div>
        <div class="main__donate-logo">
          <svg xmlns="http://www.w3.org/2000/svg" width="146" height="124" viewBox="0 0 146 124"><g transform="translate(-138 -2180)"><rect width="146" height="124" transform="translate(138 2180)" fill="none"/><g transform="translate(144.977 2190.98)"><g transform="translate(0.024 0.019)"><path d="M72.7,62.608c-.295,0-10.475,1.139-10.77,1.126-1.646-.074-6.964-.337-7.943-.723l-.032-.016a.665.665,0,0,1,.234-1.276l5.736-.3c-.426-.878-7.251-2.656-8.895-3.282-1.026-.389-.884-.71-.631-.9a.976.976,0,0,1,.576-.179h6.446s-7.183-2.283-7.327-2.322-1.276-.834-.526-1.2,9.568.734,9.568.734a8.652,8.652,0,0,0-3.661-1.983c-2.317-.744-2.7-1.9-1.194-2a3.238,3.238,0,0,1,1.12.134A69.491,69.491,0,0,0,64.74,51.8a20.981,20.981,0,0,1-3.4-2.483c-3.085-2.777-.471-2.577-.116-2.222a31.426,31.426,0,0,0,5.481,2.63c2.917,1.128,6.049,1.289,9.158,1.5,23.228,1.554,37.083-4,41.407-5.768,2.982-1.491,11.2-4.8,15.517-11a36.415,36.415,0,0,0-2.175-10.775,35.837,35.837,0,0,0-8.356-13.1,36.334,36.334,0,0,0-51.233,0l-4.61,4.6-4.589-4.6a36.331,36.331,0,0,0-51.233,0,35.768,35.768,0,0,0-6.2,8.29A36.513,36.513,0,0,0,2.814,50.189a27.9,27.9,0,0,0,6.775,7.49c13.063,10.57,20.225,14.6,36.926,16.474,2.806.316,12.3,1.315,12.687,1.315,5.744-.379,10.915-3.992,13.789-5.189a30.851,30.851,0,0,0,5.4-2.754c.35-.363,2.954-.631-.058,2.225a22.076,22.076,0,0,1-3.356,2.588,69.41,69.41,0,0,0,9.308-1.615,3.206,3.206,0,0,1,1.112-.16c1.51.063,1.157,1.226-1.147,2.025a8.611,8.611,0,0,0-3.606,2.075s8.795-1.315,9.544-.973-.363,1.176-.508,1.218-7.267,2.5-7.267,2.5l6.449-.147a.971.971,0,0,1,.579.163c.263.181.408.5-.61.915-1.628.665-8.416,2.614-8.808,3.5L85.764,82a.665.665,0,0,1,.263,1.27l-.034.016c-.97.413-6.278.807-7.922.921-.292.021-.579.047-.873.082-2.52.284-17.266,4.016-43.4.739l7.006,7c.221.221.439.439.665.652s.452.421.681.629.46.413.692.61.468.395.7.587.476.381.715.568.481.368.726.547.489.352.736.526.484.342.747.5.5.329.757.487.5.31.768.463.526.3.789.444.526.284.789.423.526.284.789.4.526.263.789.381.526.242.807.36l.813.342c.263.11.55.218.823.321s.547.2.826.3.555.184.834.279.558.179.839.263.56.163.844.239.563.147.847.216l.852.2c.284.06.571.118.855.174s.573.108.86.155.576.092.863.134.576.079.863.113l.868.095c.289.029.579.053.868.074s.576.039.868.053.581.029.871.029H67.3c.289,0,.581-.016.871-.029s.576-.032.868-.053.579-.045.871-.071.576-.058.865-.092.576-.071.865-.113.573-.087.86-.134c.573-.095,1.147-.205,1.715-.329.287-.06.568-.126.852-.195s.568-.139.85-.216.565-.153.844-.239l.836-.263c.279-.089.558-.181.834-.279s.563-.195.826-.3l.821-.321c.287-.11.542-.226.813-.342s.547-.242.81-.36.526-.25.789-.381.526-.263.789-.4.539-.279.789-.423.526-.295.776-.444.5-.305.765-.463.508-.324.757-.487.5-.334.747-.5.494-.347.736-.526.489-.36.729-.547.473-.376.713-.568.473-.389.7-.586.463-.4.692-.608.458-.416.681-.629.447-.431.665-.652l30.214-30.222h0a35.915,35.915,0,0,0,4.208-5.06C106.247,64.084,90.725,62.143,72.7,62.608Z" transform="translate(-0.024 -0.019)" fill="#fff"/></g></g></g></svg>
          <svg class="donate__logo-cover" xmlns="http://www.w3.org/2000/svg" width="131.882" height="65.706" viewBox="0 0 131.882 65.706"><path d="M-6977.984-810.264c.19.732,3.114,18.924,32.719,40.152,12.185,4.752,18.545,12.612,19.085,12.432a78.325,78.325,0,0,0,36.9-9.6c14.715-8.142,29.83-23.284,34.75-28.422a17.878,17.878,0,0,0,2.981-4.15c2.344-4.527,6.81-14.686,5.043-23.531" transform="translate(6977.984 823.383)" fill="#fff"/></svg>
        </div>
        <div class="main__donate-percents">
          <p class="main__donate-percent">100%</p>
          <p class="main__donate-pertext main-black">of your donations go to rural Armenia</p>
        </div>
        <div class="main__donate-formwrapp">
          <form class="main__donate-form" action="/">
            <label class="main__donate-label main__donate-one">
              <input class="main__donate-radio"  type="radio" name="donate-amount" value="100">
              <span class="main__donate-amount main-black">100</span>
            </label>
            <label class="main__donate-label main__donate-five">
              <input class="main__donate-radio"  type="radio" name="donate-amount" value="500">
              <span class="main__donate-amount main-black">500</span>
            </label>
            <label class="main__donate-label main__donate-custom main__donate-amount main-black">
              <input type="text" name="donate-custom" placeholder="Custom Amount">          
            </label>
            <button class="main__donate-submit" type="submit">Donate</button>
          </form>
        </div>
      </div>
    </section>
    <section class="main__villages">
      <div class="main__villages-text container">
        <h2 class="main__villages-title title">Empowering Armenia's Villages</h2>
        <p class="main__villages-descr">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's text.</p>
        <a class="main__villages-more link-more" href="#">Read more</a>
      </div>
      <div class="main__villages-vidwrapper">
        <video class="main__villages-video" src="#" poster="<?php echo get_template_directory_uri(); ?>/imgs/poster.jpg"></video>
        <button class="main__villages-vidplay" type="button"></button>
      </div>
    </section>
    <section class="main__stories container">
      <div>
        <h2 class="main__stories-title title">Stories</h2>
        <div class="main__stories-arrows">
          <button class="main__stories-arrow main__stories-prev" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 52 52"><g class="arrow-bg" transform="translate(50 50) rotate(180) scale(.9)"><rect width="52" height="52" rx="26" stroke="none"/><rect x="0.5" y="0.5" width="51" height="51" rx="25.5" fill="none"/></g><g transform="translate(15 18)"><g transform="translate(20.999 0.999) rotate(90)"><path d="M14.693,12.96,7.7,19.953a.413.413,0,0,1-.584,0L.121,12.96a.413.413,0,0,1,0-.584l1.863-1.864a.412.412,0,0,1,.584,0l3,3V.413A.413.413,0,0,1,5.979,0H8.834a.413.413,0,0,1,.413.413v13.1l3-3a.425.425,0,0,1,.584,0l1.864,1.864a.413.413,0,0,1,0,.584Z"/></g><rect width="22" height="16" fill="none"/></g></svg>
          </button>
          <button class="main__stories-arrow main__stories-next" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 52 52"><g class="arrow-bg" transform="translate(50 50) rotate(180) scale(.9)"><rect class="arrow-bg" width="52" height="52" rx="26" stroke="none"/><rect x="0.5" y="0.5" width="51" height="51" rx="25.5" fill="none"/></g><g transform="translate(15 18)"><g transform="translate(2 15) rotate(270)"><path d="M14.693,12.96,7.7,19.953a.413.413,0,0,1-.584,0L.121,12.96a.413.413,0,0,1,0-.584l1.863-1.864a.412.412,0,0,1,.584,0l3,3V.413A.413.413,0,0,1,5.979,0H8.834a.413.413,0,0,1,.413.413v13.1l3-3a.425.425,0,0,1,.584,0l1.864,1.864a.413.413,0,0,1,0,.584Z"/></g><rect width="22" height="16" fill="none"/></g></svg>
          </button>
        </div>
      </div>
      <div class="main__stories-items invisible-scroll">        
        <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <img class="main__stories-childpic" src="<?php echo get_template_directory_uri(); ?>/imgs/child-1.jpg" alt="name">
            <div class="main__stories-info">
              <p class="main__stories-date">Sep 21st 2021</p>
              <p class="main__stories-name">Lorem Ipsum</p>
              <p class="main__stories-descr">Lorem Ipsum is just a simply dummy text</p>
            </div>
          </div>
        </div>
        <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <img class="main__stories-childpic" src="<?php echo get_template_directory_uri(); ?>/imgs/child-2.jpg" alt="name">
            <div class="main__stories-info">
              <p class="main__stories-date">Sep 21st 2021</p>
              <p class="main__stories-name">Lorem Ipsum</p>
              <p class="main__stories-descr">Lorem Ipsum is just a simply dummy text</p>
            </div>
          </div>
        </div>
        <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <img class="main__stories-childpic" src="<?php echo get_template_directory_uri(); ?>/imgs/child-3.jpg" alt="name">
            <div class="main__stories-info">
              <p class="main__stories-date">Sep 21st 2021</p>
              <p class="main__stories-name">Lorem Ipsum</p>
              <p class="main__stories-descr">Lorem Ipsum is just a simply dummy text</p>
            </div>
          </div>
        </div>
        <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <img class="main__stories-childpic" src="<?php echo get_template_directory_uri(); ?>/imgs/child-4.jpg" alt="name">
            <div class="main__stories-info">
              <p class="main__stories-date">Sep 21st 2021</p>
              <p class="main__stories-name">Lorem Ipsum</p>
              <p class="main__stories-descr">Lorem Ipsum is just a simply dummy text</p>
            </div>
          </div>          
        </div>
        <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <img class="main__stories-childpic" src="<?php echo get_template_directory_uri(); ?>/imgs/child-1.jpg" alt="name">
            <div class="main__stories-info">
              <p class="main__stories-date">Sep 21st 2021</p>
              <p class="main__stories-name">Lorem Ipsum</p>
              <p class="main__stories-descr">Lorem Ipsum is just a simply dummy text</p>
            </div>
          </div>
        </div>
        <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <img class="main__stories-childpic" src="<?php echo get_template_directory_uri(); ?>/imgs/child-2.jpg" alt="name">
            <div class="main__stories-info">
              <p class="main__stories-date">Sep 21st 2021</p>
              <p class="main__stories-name">Lorem Ipsum</p>
              <p class="main__stories-descr">Lorem Ipsum is just a simply dummy text</p>
            </div>
          </div>
        </div>
        <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <img class="main__stories-childpic" src="<?php echo get_template_directory_uri(); ?>/imgs/child-3.jpg" alt="name">
            <div class="main__stories-info">
              <p class="main__stories-date">Sep 21st 2021</p>
              <p class="main__stories-name">Lorem Ipsum</p>
              <p class="main__stories-descr">Lorem Ipsum is just a simply dummy text</p>
            </div>
          </div>
        </div>
        <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <img class="main__stories-childpic" src="<?php echo get_template_directory_uri(); ?>/imgs/child-4.jpg" alt="name">
            <div class="main__stories-info">
              <p class="main__stories-date">Sep 21st 2021</p>
              <p class="main__stories-name">Lorem Ipsum</p>
              <p class="main__stories-descr">Lorem Ipsum is just a simply dummy text</p>
            </div>
          </div>          
        </div>
      </div>
      <div class="main__stories-morewrap">
        <a class="main__stories-more link-more" href="#">See More</a>
      </div>
    </section>
    <div class="main__location">
      <div class="main__location-img">
        <img src="<?php echo get_template_directory_uri(); ?>/imgs/coaf.jpg" alt="coaf smart center">
      </div>
      <div class="main__location-location">
        <img src="<?php echo get_template_directory_uri(); ?>/imgs/location.svg" alt="coaf smart center">
        <p class="main__location-name">COAF SMART Center</p>
      </div>
    </div>
  </main>

<?php get_footer(); ?>