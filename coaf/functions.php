<?php 
/*--------------------------------------------------------------
# THEME INSTALL
--------------------------------------------------------------*/

add_action( 'after_setup_theme', 'coaf_setup' );
function coaf_setup() {

add_theme_support(
 'custom-logo',
 array(
     'height'      => 36.4,
     'width'       => 100,
     'flex-width'  => false,
     'flex-height' => false,
 )
);

load_theme_textdomain( 'coaf', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'responsive-embeds' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'html5', array( 'search-form' ) );
 
 add_image_size( 'coaf-image-278x200-cropped', 278, 200, true ); 
 add_image_size( 'coaf-image-1162x442-cropped', 1162, 442, true );  

register_nav_menus(
    array(
        'menu-1' => __( 'Main menu', 'sadvik' ),
    )
);

/*--------------------------------------------------------------
# Register Sidebars
--------------------------------------------------------------*/
function coaf_register_sidebars() {

    // Archive Sidebar
    register_sidebar(array(
            'name'          => esc_html__('Archives Sidebar', 'coaf'),
            'description'   => esc_html__('Sidebar for the archives.', 'coaf'),
            'id'            => 'archive-sidebar',
            'before_widget' => '<aside id="%1$s" class="widget %2$s">',
            'after_widget'  => '</aside>',
            'before_title'  => '<div class="widget_title">',
            'after_title'   => '</div>',
        )
    );

    // Footer 1
    register_sidebar(array(
            'name'          => esc_html__('Footer Column 1', 'coaf'),
            'description'   => esc_html__('This is the first column for the footer.', 'coaf'),
            'id'            => 'footer-col-1',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="footer-bottom__quick-title">',
            'after_title'   => '</div>',
        )
    );
    // Footer 2
    register_sidebar(array(
        'name'          => esc_html__('Footer Column 2', 'coaf'),
        'description'   => esc_html__('This is the first column for the footer.', 'coaf'),
        'id'            => 'footer-col-2',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<div class="widget_title">',
        'after_title'   => '</div>',
    )
);
}
add_action('widgets_init', 'coaf_register_sidebars');


/*--------------------------------------------------------------
# CSS JAVASCRIPT load
--------------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', 'coaf_enqueue' );
function coaf_enqueue() {

    wp_enqueue_style( 'coaf-responsive', get_template_directory_uri() . '/assets/css/responsive.css', array(), '1.0', 'all' );
    wp_enqueue_style( 'coaf-fonts', get_template_directory_uri() . '/assets/css/fonts.min.css', array(), '1.0', 'all' );
    wp_enqueue_style( 'coaf-style', get_template_directory_uri() . '/style.css', array(), '1.0', 'all' );

    wp_enqueue_script( 'tactun-slick-js', get_template_directory_uri() . '/assets/js/slick.min.js', array( 'jquery' ), '1', true );
    wp_enqueue_script( 'tactun-main-js', get_template_directory_uri() . '/assets/js/main.min.js', array( 'jquery' ), '1', true );
    wp_enqueue_script( 'tactun-scrollcount-js', get_template_directory_uri() . '/assets/js/scrollcount.js', array( 'jquery' ), '1', true );

}

/*--------------------------------------------------------------
# SVG support
--------------------------------------------------------------*/

function add_file_types_to_uploads($file_types){
    $new_filetypes = array();
    $new_filetypes['svg'] = 'image/svg+xml';
    $file_types = array_merge($file_types, $new_filetypes );
    return $file_types;
}
add_filter('upload_mimes', 'add_file_types_to_uploads');



function randomik($len=6) {
	$s = '';
	$b="QWERTYUPASDFGHJKLZXCVBNMqwertyuopasdfghjkzxcvbnm";
	while($len-->0) $s.=$b[mt_rand(0,strlen($b)-1)];
	return $s;
}

/*--------------------------------------------------------------
# Remove Files
--------------------------------------------------------------*/

function remove_plugin_styles() {
    wp_deregister_style( 'js_composer_front-css' ); // js composer (wpbakery)
    wp_deregister_style( 'js_composer_front' ); // js composer (wpbakery)
  }
  add_action( 'wp_enqueue_scripts', 'remove_plugin_styles', 100 );

  function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	
	// Remove from TinyMCE
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter out the tinymce emoji plugin.
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/*--------------------------------------------------------------
# Load Files
--------------------------------------------------------------*/

require_once( get_template_directory() . '/inc/theme_functionality.php' );
require_once( get_template_directory() . '/inc/theme-option.php' );
require_once( get_template_directory() . '/inc/theme-options.php' );

if ( class_exists( 'WPBakeryShortCode' ) ) {
    require_once( get_template_directory() . '/inc/visual_composer.php' );
}

}