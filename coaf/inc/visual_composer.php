<?php
/**
 * Visual Composer Extensions
 */

// don't load directly
if ( !defined( 'ABSPATH' ) )
    die( '-1' );

 if (class_exists('WPBakeryShortCode')) {
    // vc_add_param("vc_section", array(
    //     "type" => "checkbox",
    //     "class" => "",
    //     "heading" => "Angle top",
    //     "param_name" => "angle_top",
    //     'group'       => esc_html__( 'Design Options', 'coaf' ),
    // ));
    // vc_add_param("vc_section", array(
    //     "type" => "checkbox",
    //     "class" => "",
    //     "heading" => "Angle Bottom",
    //     "param_name" => "angle_bottom",
    //     'group'       => esc_html__( 'Design Options', 'coaf' ),
    // ));

        // Remove Elements
    vc_remove_element('vc_wp_search');
    vc_remove_element('vc_wp_meta');
    vc_remove_element('vc_wp_recentcomments');
    vc_remove_element('vc_wp_calendar');
    vc_remove_element('vc_wp_pages');
    vc_remove_element('vc_wp_tagcloud');
  //  vc_remove_element('vc_wp_custommenu');
    vc_remove_element('vc_wp_text');
    vc_remove_element('vc_wp_posts');
    vc_remove_element('vc_wp_links');
    vc_remove_element('vc_wp_categories');
    vc_remove_element('vc_wp_archives');
    vc_remove_element('vc_wp_rss');
    vc_remove_element('vc_carousel');
    vc_remove_element('vc_posts_slider');
    vc_remove_element('vc_pinterest');
    vc_remove_element('vc_tweetmeme');
    vc_remove_element('vc_facebook');
    vc_remove_element('vc_flickr');
    vc_remove_element('vc_progress_bar');
    vc_remove_element('vc_pie');
    vc_remove_element('vc_round_chart');
    vc_remove_element('vc_line_chart');

    /**
     * Activate VC on custom post types
     */
    if (function_exists('vc_set_default_editor_post_types')) {
        vc_set_default_editor_post_types(array(
            'page',
            'post',
        ));
    }

    /**
     * Add new Google Fonts
     */
    if (!function_exists('coaf_vc_add_google_fonts')) {
        function coaf_vc_add_google_fonts($fonts)
        {
            $fonts[] = (object)array(
                'font_family' => 'Poppins',
                'font_styles' => '300,regular,500,600,700',
                'font_types' => '300 light:300:normal,400 regular:400:normal,500 medium:500:normal,600 semi-bold:600:normal,700 bold:700:normal'
            );
            return $fonts;
        }
    }
    add_filter('vc_google_fonts_get_fonts_filter', 'coaf_vc_add_google_fonts', 10, 1);


    /**
     * Filter to Replace Class Names
     */
    if( !function_exists('coaf_vc_custom_classes') ) {

        function coaf_vc_custom_classes($class_string, $tag) {
            if ($tag == 'vc_row' || $tag == 'vc_row_inner') {
                $class_string = str_replace('vc_row-fluid', 'row-fluid', $class_string);
                $class_string = str_replace('vc_row', 'row', $class_string);
                $class_string = str_replace('wpb_row', '', $class_string);
                $class_string = str_replace('vc_inner', '', $class_string);
            }
            if ($tag == 'vc_column' || $tag == 'vc_column_inner') {
                $class_string = preg_replace('/vc_col-sm-(\d{1,2})/', 'col-md-$1', $class_string);
                $class_string = str_replace('wpb_column', '', $class_string);
                $class_string = str_replace('vc_column_container', '', $class_string);
                $class_string = str_replace('vc_column-inner', 'column-inner', $class_string);
                $class_string = str_replace('wpb_wrapper', 'wrapper', $class_string); 
            }
            if ($tag == 'vc_section') {
                $class_string = str_replace('vc_section', 'section', $class_string);          
            }
 
            return $class_string;
        }
        add_filter('vc_shortcodes_css_class', 'coaf_vc_custom_classes', 10, 2);
    }

    $news_categories_array = get_terms( 'category', array(
        'hide_empty' => false,
         ) );
        $news_categories = array(
            __( 'All', 'debet' ) => 'all'
        );
        if( $news_categories_array && ! is_wp_error( $news_categories_array )  ){
            foreach( $news_categories_array as $cat ){
                $news_categories[$cat->name] = $cat->term_id;
            }
        }

    /*---------------------------------------------------------------------------------
        CUSTOM HEADING
    -----------------------------------------------------------------------------------*/
    class WPBakeryShortCode_Coaf_Custom_Heading extends WPBakeryShortCode{}

    vc_map( array(

        'name' => esc_html__( 'Custom Heading', 'coaf'),
        'description' => esc_html__( 'Create custom headings with optionals.', 'coaf' ),
        'base' => 'coaf_custom_heading',
        'icon' => 'icon-wpb-toggle-small-expand',
        'category' => esc_html( 'Coaf' ),
        'params' => array(

            // The Title
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Heading Title', 'coaf' ),
                'description' => esc_html__( 'Enter the title for your custom heading.', 'coaf' ),
                'param_name' => 'heading_title'
            ),

            // The font size
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Font Size', 'coaf' ),
                'description' => esc_html__( 'Enter the font-size in px. Example: 30', 'coaf' ),
                'param_name' => 'heading_size'
            ),

            // The font size
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Line hright', 'coaf' ),
                'description' => esc_html__( 'Enter the Line height in px. Example: 30', 'coaf' ),
                'param_name' => 'heading_line'
            ),

            // Font Weight
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Font Weight', 'coaf' ),
                'description' => esc_html__( 'Select the font weight for your heading.', 'coaf' ),
                'param_name' => 'heading_weight',
                'value' => array(
                    esc_html__( 'Theme Default', 'coaf' ) => 'normal',
                    esc_html__( 'Light', 'coaf' ) => '300',
                    esc_html__( 'Normal', 'coaf' ) => '500',
                    esc_html__( 'Semi Bold', 'coaf' ) => '600',
                    esc_html__( 'Bold', 'coaf' ) => '900',
                ),
                'std' => 'normal',
                'save_always' => true,
            ),

            // Text Transform
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Text Transform', 'coaf' ),
                'description' => esc_html__( 'Select the transform for your heading.', 'coaf' ),
                'param_name' => 'heading_transform',
                'value' => array(
                    esc_html__( 'None', 'coaf' ) => 'none',
                    esc_html__( 'Capitalize', 'coaf' ) => 'capitalize',
                    esc_html__( 'Uppercase', 'coaf' ) => 'uppercase',
                    esc_html__( 'Lowercase', 'coaf' ) => 'lowercase',
                ),
                'std' => 'theme_default',
                'save_always' => true,
            ),

            // Letter spacing
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Letter Spacing', 'coaf' ),
                'description' => esc_html__( '(Optional) Enter the letter-spacing in px. Example: -0.5', 'coaf' ),
                'param_name' => 'heading_spacing'
            ),

            // Text Alignment
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Text Alignment', 'coaf' ),
                'description' => esc_html__( 'Left, Right or Center your heading.', 'coaf' ),
                'param_name' => 'heading_alignnment',
                'value' => array(
                    esc_html__( 'Left', 'coaf' ) => 'left',
                    esc_html__( 'Center', 'coaf' ) => 'center',
                    esc_html__( 'Right', 'coaf' ) => 'right',
                ),
                'std' => 'left',
                'save_always' => true,
            ),

            // The heading tag (h1,h2,h3)
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Heading Tag', 'coaf' ),
                'description' => esc_html__( 'Would you like to use an h1, h2, h3, h4 tag?', 'coaf' ),
                'param_name' => 'heading_tag',
                'value' => array(
                    esc_html__( 'H1 Tag', 'coaf' ) => 'h1',
                    esc_html__( 'H2 Tag', 'coaf' ) => 'h2',
                    esc_html__( 'H3 Tag', 'coaf' ) => 'h3',
                    esc_html__( 'H4 Tag', 'coaf' ) => 'h4',
                    esc_html__( 'DIV Tag', 'coaf' ) => 'div',
                    esc_html__( 'SPAN Tag', 'coaf' ) => 'span',
                    esc_html__( 'P Tag', 'coaf' ) => 'p',
                ),
                'std' => 'h2',
                'save_always' => true,
            ),

            // The Title
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Custom Class', 'coaf' ),
                'description' => esc_html__( '(Optional) Add a class and refer to it in your CSS.', 'coaf' ),
                'param_name' => 'custom_class'
            ),

            // Normal Color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Heading Color', 'coaf' ),
                'description' => esc_html__( 'Choose your heading\'s color', 'coaf' ),
                'param_name' => 'heading_color',
                'group'       => esc_html__( 'Colors', 'coaf' ),
                'dependency'  => array( 'element' => 'heading_color_type', 'value' => 'normal' ),
            ),

        )
    ));

    /*---------------------------------------------------------------------------------
        CUSTOM BUTTON
    -----------------------------------------------------------------------------------*/
    class WPBakeryShortCode_Coaf_Custom_button extends WPBakeryShortCode{}

    vc_map( array(

        'name' => esc_html__( 'Custom Button', 'coaf' ),
        'description' => esc_html__( 'Add in a custom button with multiple different styles', 'coaf' ),
        'category' => esc_html( 'Coaf' ),
        'base' => 'coaf_custom_button',
        'params' => array(

            // Button text
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Button Text', 'coaf' ),
                'param_name' => 'custom_button_text',
            ),

            // Button link
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Button Link', 'coaf' ),
                'param_name' => 'custom_button_link'
            ),

            // Button target
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Link Target', 'coaf' ),
                'param_name' => 'custom_button_target',
                'value' => array(
                    esc_html__( 'Open in Same Window', 'coaf' ) => '_self',
                    esc_html__( 'Open in New Window', 'coaf' ) => '_blank',
                ),
            ),

            // Button size
            array(
                'type'=> 'dropdown',
                'heading' => esc_html__( 'Button Size', 'coaf' ),
                'param_name' => 'custom_button_size',
                'value' => array(
                    esc_html__( 'Small', 'coaf' ) => 'size_small',
                    esc_html__( 'Medium', 'coaf' ) => 'size_medium',
                    esc_html__( 'Large', 'coaf' ) => 'size_large',
                ),
                'save_always' => true,
                'group' => esc_html__( 'Button Style', 'coaf' ),
            ),

            // Button corners
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Button Corner Style', 'coaf' ),
                'param_name' => 'custom_button_corners',
                'value' => array(
                    esc_html__( 'Round', 'coaf' ) => 'corners_round',
                    esc_html__( 'Rounded', 'coaf' ) => 'corners_rounded',
                    esc_html__( 'Square', 'coaf' ) => 'corners_square',
                ),
                'save_always' => true,
                'group' => esc_html__( 'Button Style', 'coaf' ),
            ),

            /**
             * Category - Button Position
             */

            // Button aligment
            array(
                'type' => 'dropdown',
                'heading' => esc_html__( 'Button Alignment', 'coaf' ),
                'param_name' => 'custom_button_alignment',
                'category' => esc_html__( 'Position', 'coaf' ),
                'value' => array(
                    esc_html__( 'Left', 'coaf' ) => 'alignment_left',
                    esc_html__( 'Center', 'coaf' ) => 'alignment_center',
                    esc_html__( 'Right', 'coaf' ) => 'alignment_right',
                ),
                'save_always' => true,
                'group' => esc_html__( 'Button Style', 'coaf' ),
            ),


            /**
             * Button Colors
             */

            // Background color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Background Color', 'coaf' ),
                'param_name' => 'button_background_color',
                'group' => esc_html__( 'Button Colors', 'coaf' ),
            ),

            // Text color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Text Color', 'coaf' ),
                'param_name' => 'button_text_color',
                'group' => esc_html__( 'Button Colors', 'coaf' ),
            ),

            // Background hover color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Background Hover Color', 'coaf' ),
                'param_name' => 'button_background_color_hov',
                'group' => esc_html__( 'Button Colors', 'coaf' ),
            ),

            // Text hover color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Text Hover Color', 'coaf' ),
                'param_name' => 'button_text_color_hov',
                'group' => esc_html__( 'Button Colors', 'coaf' ),
            ),


            /**
             * Icon Options
             */

            // choose icon
            array(
                'heading'    => esc_html__( 'Icon', 'coaf' ),
                'type'       => 'iconpicker',
                'param_name' => 'button_icon',
                'value'      => '',
                'weight'     => 1,
                'group' => esc_html__( 'Button Icon', 'coaf' ),
                'save_always' => true,
            ),

            // icon color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Icon Color', 'coaf' ),
                'param_name' => 'button_icon_color',
                'group' => esc_html__( 'Button Icon', 'coaf' ),
            ),

            // icon hover color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Icon Hover Color', 'coaf' ),
                'param_name' => 'button_icon_color_hov',
                'group' => esc_html__( 'Button Icon', 'coaf' ),
            ),

        )

    ));

    /*---------------------------------------------------------------------------------
        Hero slider
    -----------------------------------------------------------------------------------*/
    class WPBakeryShortCode_Coaf_Heroslider extends WPBakeryShortCode {}

    vc_map( array(

        'name' => esc_html__( 'Hero Slider', 'coaf' ),
        'description' => esc_html__( 'Add slider.', 'coaf' ),
        'base' => 'coaf_heroslider',
        'icon' => 'icon-wpb-toggle-small-expand',
        'category' => esc_html( 'Coaf' ),
        'params' => array(
            array(
                'type' => 'param_group',
                'value' => '',
                'param_name' => 'coaf_slider_slider',
                'params' => array(
                    // Image
                    array(
                        'type' => 'attach_image',
                        'heading' => esc_html__( 'Slider image', 'coaf' ),
                        'description' => esc_html__( 'Add slider image', 'coaf' ),
                        'param_name' => 'coaf_slider_img',
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__( 'Slider Title', 'coaf' ),
                        'description' => esc_html__( 'Add slider title', 'coaf' ),
                        'param_name' => 'coaf_slider_title',
                    ),
                    array(
                        'type' => 'textfield',
                        'value' => '',
                        'heading' => esc_html__( 'Slider Description', 'coaf' ),
                        'description' => esc_html__( 'Add slider description', 'coaf' ),
                        'param_name' => 'coaf_slider_desc',
                    )
                ),
            ) 
        )

    ) );

    /*---------------------------------------------------------------------------------
       IMAGE AND MAP MARKERS BLOCK
    -----------------------------------------------------------------------------------*/
    class WPBakeryShortCode_Coaf_Image_Map extends WPBakeryShortCode {}

    vc_map( array(

        'name' => esc_html__( 'Image map', 'coaf' ),
        'description' => esc_html__( 'Add image map block.', 'coaf' ),
        'base' => 'coaf_image_map',
        'category' => esc_html( 'Coaf' ),
        'params' => array(
            array(
                'type'        => 'attach_image',
                'heading'     => esc_html__('Backgraund image', 'coaf'),
                'param_name'  => 'map_image',
            ),
            array(
                'type'        => 'textarea_raw_html',
                'heading'     => esc_html__('Map svg', 'coaf'),
                'param_name'  => 'map_svg',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Block width', 'coaf' ),
                'param_name' => 'map_width',
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Block height', 'coaf' ),
                'param_name' => 'map_height',
            ),
            array(
               'type' => 'param_group',
               'param_name' => 'values',
               'params' => array(
                // Title
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Marker title', 'coaf' ),
                    'param_name' => 'marker_title',
                ),

                // Excerpt
                array(
                    'type' => 'textarea',
                    'heading' => esc_html__( 'Marker descritpion', 'coaf' ),
                    'param_name' => 'marker_description',
                ),
                // Excerpt
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__( 'Marker image', 'coaf' ),
                    'param_name' => 'marker_image',
                ),
                // position
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Marker top', 'coaf' ),
                    'param_name' => 'marker_top',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Marker left', 'coaf' ),
                    'param_name' => 'marker_left',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Marker bottom', 'coaf' ),
                    'param_name' => 'marker_bottom',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Marker right', 'coaf' ),
                    'param_name' => 'marker_right',
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html( 'Marker position', 'coaf' ),
                    'description' => esc_html__( 'Select marker orientation', 'coaf' ),
                    'param_name' => 'marker_position',
                    'value' => array(
                        esc_html__( 'Top Left', 'coaf' ) => 'topleft',
                        esc_html__( 'Top Right', 'coaf' ) => 'topright',
                        esc_html__( 'Bottom Left', 'coaf' ) => 'bottomleft',
                        esc_html__( 'Bottom Right', 'coaf' ) => 'bottomright',
                    ),
                    'save_always' => true,

                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_html( 'Marker orientation', 'coaf' ),
                    'description' => esc_html__( 'Select marker orientation', 'coaf' ),
                    'param_name' => 'marker_orientation',
                    'value' => array(
                        esc_html__( 'Top', 'coaf' ) => 'top',
                        esc_html__( 'Left', 'coaf' ) => 'left',
                        esc_html__( 'Bottom', 'coaf' ) => 'bottom',
                        esc_html__( 'Right', 'coaf' ) => 'right',
                    ),
                    'save_always' => true,

                ),
                // Color
                array(
                    'type' => 'colorpicker',
                    'heading' => esc_html__( 'Marker color', 'coaf' ),
                    'param_name' => 'marker_color',
                ),
            ),
        ),

        ),
    ) );
   /*---------------------------------------------------------------------------------
       COUNTER BLOCK
    -----------------------------------------------------------------------------------*/
    class WPBakeryShortCode_Coaf_Counter_Block extends WPBakeryShortCode {}

    vc_map( array(

        'name' => esc_html__( 'Counter block', 'coaf' ),
        'description' => esc_html__( 'Add image map block.', 'coaf' ),
        'base' => 'coaf_counter_block',
        'category' => esc_html( 'Coaf' ),
        'params' => array(
            array(
               'type' => 'param_group',
               'param_name' => 'values',
               'params' => array(
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Counter number', 'coaf' ),
                    'param_name' => 'counter_number',
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Counter increment', 'coaf' ),
                    'param_name' => 'counter_increment',
                ),
                // Title
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Counter title', 'coaf' ),
                    'param_name' => 'counter_title',
                ),
                // Excerpt
                array(
                    'type' => 'textfield',
                    'heading' => esc_html__( 'Counter symbol', 'coaf' ),
                    'param_name' => 'counter_symbol',
                ),
                // Image
                array(
                    'type' => 'attach_image',
                    'heading' => esc_html__( 'Counter icon', 'coaf' ),
                    'description' => esc_html__( 'Add Counter icon', 'coaf' ),
                    'param_name' => 'counter_icon',
                ),
            ),
        ),

        ),
    ) );
        /*---------------------------------------------------------------------------------
        CREATE SPACE
    -----------------------------------------------------------------------------------*/
    class WPBakeryShortCode_Coaf_Create_Space extends WPBakeryShortCode{}

    vc_map( array(

        'name' => esc_html__( 'Create Space', 'coaf' ),
        'description' => esc_html__( 'Create spacing for desktops, tablets and mobile devices,', 'coaf' ),
        'base' => 'coaf_create_space',
        'icon' => 'icon-wpb-toggle-small-expand',
        'category' => esc_html( 'Coaf' ),
        'params' => array(

            // Desktop Spacing
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Desktop Spacing', 'coaf' ),
                'description' => esc_html__( 'Enter the spacing in px for Desktop Sized Devices.', 'coaf' ),
                'param_name' => 'create_space_desktop',
            ),

            // Tablet Spacing
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Tablet Spacing', 'coaf' ),
                'description' => esc_html__( 'Enter the spacing in px for Tablet Sized Devices.', 'coaf' ),
                'param_name' => 'create_space_tablet',
            ),

            // Mobile Spacing
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Mobile Spacing', 'coaf' ),
                'description' => esc_html__( 'Enter the spacing in px for Mobile Sized Devices.', 'coaf' ),
                'param_name' => 'create_space_mobile'
            ),
            // Color
            array(
                'type' => 'colorpicker',
                'heading' => esc_html__( 'Color block', 'coaf' ),
                'description' => esc_html__( 'Add color block', 'coaf' ),
                'param_name' => 'create_space_color',
                'std' => '',
                'save_always' => true,
            ),
        )

    ) );

    /*---------------------------------------------------------------------------------
        VIDEO
    -----------------------------------------------------------------------------------*/
    class WPBakeryShortCode_Coaf_Video extends WPBakeryShortCode{}

    vc_map( array(

        'name' => esc_html__( 'Video Block', 'coaf' ),
        'description' => esc_html__( 'Add video block,', 'coaf' ),
        'base' => 'coaf_video',
        'icon' => 'icon-wpb-film-youtube',
        'category' => esc_html( 'Coaf' ),
        'params' => array(
            // Desktop Spacing
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Video link', 'coaf' ),
                'description' => esc_html__( 'Add video link from Youtube or Vimeo.', 'coaf' ),
                'param_name' => 'video_link',
            ),
            // Image
            array(
                'type' => 'attach_image',
                'heading' => esc_html__( 'Select video', 'coaf' ),
                'description' => esc_html__( 'Add video file', 'coaf' ),
                'param_name' => 'video_file',
            ),
            // Image
            array(
                'type' => 'attach_image',
                'heading' => esc_html__( 'Select video poster', 'coaf' ),
                'description' => esc_html__( 'Add video poster', 'coaf' ),
                'param_name' => 'video_poster',
            ),
            // Heighgt
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Video block height', 'coaf' ),
                'description' => esc_html__( 'Add height px from block.', 'coaf' ),
                'param_name' => 'video_height',
            ),
            )

    ) );
        /*---------------------------------------------------------------------------------
        Blog Slider
    -----------------------------------------------------------------------------------*/
    class WPBakeryShortCode_coaf_News_list extends WPBakeryShortCode {}

    vc_map( array(

        'name' => esc_html__( 'News slider', 'coaf' ),
        'description' => esc_html__( 'Display news slider.', 'coaf' ),
        'base' => 'coaf_news_slider',
        'category' => esc_html( 'Coaf' ),
        'params' => array(
            array(
                'type' => 'dropdown',
                'heading' => __( 'Category', 'coaf' ),
                'param_name' => 'bloggrid_block_category',
                'value' => $news_categories,
            ),
            // Number of coaf
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Total Number of News', 'coaf' ),
                'description' => esc_html__( 'How many coaf would you like to display? (-1 means all)', 'coaf' ),
                'param_name' => 'news_number',
                'std' => '-1',
                'save_always' => true,
            ),
            // Description Lenght
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Description lenght', 'coaf' ),
                'param_name' => 'news_block_description_lenght',
                'std' => '25',
            ),
            // Link
            array(
                'type' => 'checkbox',
                'heading' => esc_html__( 'Enable Custom Link', 'coaf' ),
                'param_name' => 'news_block_enable_link',
            ),

            // Link Text
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Link Text', 'coaf' ),
                'param_name' => 'news_block_link_text',
                'dependency' => array( 'element' => 'news_block_enable_link', 'value' => 'true' ),
            ),

            // Link URL
            array(
                'type' => 'textfield',
                'heading' => esc_html__( 'Link URL', 'coaf' ),
                'param_name' => 'news_block_link_url',
                'dependency' => array( 'element' => 'news_block_enable_link', 'value' => 'true' ),
            ),
        ),
    ) );

 } // End Class_exists

?>