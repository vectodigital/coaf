<?php

// Returns theme options data

if ( ! function_exists( 'coaf_option' ) ) {
	function coaf_option( $id, $fallback = false, $param = false ) {
		if ( isset( $_GET['coaf_'.$id] ) ) {
			if ( '-1' == $_GET['coaf_'.$id] ) {
				return false;
			} else {
				return $_GET['coaf_'.$id];
			}
		} else {
			global $coaf_options;
			if ( $fallback == false ) $fallback = '';
			$output = ( isset($coaf_options[$id]) && $coaf_options[$id] !== '' ) ? $coaf_options[$id] : $fallback;
			if ( !empty($coaf_options[$id]) && $param ) {
				$output = $coaf_options[$id][$param];
			}
		}
		return $output;
	}
}

/*--------------------------------------------------------------
# Outputs favicon to head
--------------------------------------------------------------*/
if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ) {

    function coaf_output_favicon() {
$protocols = array( 'https' ); 
        $favicons = null;

        if (coaf_option('favicon', '', 'url')) $favicons .= '
	<link rel="shortcut icon" href="' . esc_url(coaf_option('favicon', 'https', 'url')) . '">';

        if (coaf_option('iphone_icon', '', 'url')) $favicons .= '
	<link rel="apple-touch-icon-precomposed" href="' . esc_url(coaf_option('iphone_icon', '', 'url')) . '">';

        if (coaf_option('ipad_icon', '', 'url')) $favicons .= '
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="' . esc_url(coaf_option('ipad_icon', '', 'url')) . '">';

        echo $favicons;
    }

    add_action('wp_head', 'coaf_output_favicon');

}