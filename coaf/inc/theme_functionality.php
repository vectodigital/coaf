<?php
// Exit if accessed directly
if( !defined( 'ABSPATH' ) ) {
    die;
}

    // Load Redux Extensions
    if (file_exists(dirname(__FILE__) . '/redux/ReduxCore/inc/loader.php')) {
        require_once dirname(__FILE__) . '/redux/ReduxCore/inc/loader.php';
    }

    // Load Redux Framework
    if (!class_exists('ReduxFramework')) {
        require_once dirname(__FILE__) . '/redux/ReduxCore/framework.php';
    }
    // Load Extra Functions
  //  require_once dirname(__FILE__) . '/additional_functions.php';
?>