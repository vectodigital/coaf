<?php

/**
* For full documentation, please visit: http://docs.reduxframework.com/
* For a more extensive sample-config file, you may look at:
* https://github.com/reduxframework/redux-framework/blob/master/sample/sample-config.php
*/

if ( ! class_exists( 'Redux' ) ) {
return;
}

$opt_name = 'coaf_options';
$theme    = wp_get_theme();

$args = array(
'opt_name'              => $opt_name,
'dev_mode'              => FALSE,
'use_cdn'               => FALSE,
'display_name'          => $theme->get( 'Name' ),
'display_version'       => $theme->get( 'Version' ),
'page_slug'             => 'theme-options',
'page_title'            => 'Theme Options',
'update_notice'         => TRUE,
'redux_training'        => true,
'admin_bar'             => TRUE,
'menu_type'             => 'menu',
'page_parent'           => '',
'menu_title'            => 'Theme Options',
'allow_sub_menu'        => FALSE,
'page_parent_post_type' => 'your_post_type',
'default_mark'          => '*',
'class'                 => 'theme-options',
'output'                => TRUE,
'output_tag'            => TRUE,
'settings_api'          => TRUE,
'cdn_check_time'        => '1440',
'compiler'              => TRUE,
'page_permissions'      => 'manage_options',
'save_defaults'         => TRUE,
'show_import_export'    => TRUE,
'database'              => 'options',
'transient_time'        => '3600',
'network_sites'         => TRUE,
);


Redux::setArgs( $opt_name, $args );

/*--------------------------------------------------------*/
/* Layout
/*--------------------------------------------------------*/

Redux::setSection( $opt_name, array(
    'title'      => esc_html__( 'Layout', 'coaf' ),
    'id'         => 'layout_options',
    'subsection' => false,
));

/**
 * Logos
 */
Redux::setSection( $opt_name, array(
    'title'  => esc_html__( 'Logos & Favicon', 'coaf' ),
    'id'     => 'logo-favicon',
    'desc'   => '',
    'subsection' => true,
    'fields'        => array(
        array(
            'id'        => 'custom_logo',
            'url'       => true,
            'type'      => 'media',
            'title'     => esc_html__( 'Logo', 'coaf' ),
            'read-only' => false,
            'default'   => array( 'url' => get_template_directory_uri() . '/assets/imgs/logo.svg' ),
            'subtitle'  => esc_html__( 'Upload your custom site logo.', 'coaf' ),
        ),
        array(
            'title'            => esc_html__('Logo Margin', 'coaf'),
            'subtitle'         => esc_html__('Move your logo around to fit it perfectly.', 'coaf'),
            'id'               => 'logo_margin',
            'type'             => 'spacing',
            'output'           => array('.header__logo'),
            'mode'             => 'margin',
            'units'            => array('px'),
            'units_extended'   => 'false',
            'default'          => array(
                'margin-top'     => '0px',
                'margin-right'   => '0px',
                'margin-bottom'  => '0px',
                'margin-left'    => '0px',
                'units'          => 'px',
            )
        ),

        array(
            'id'    => 'favicon',
            'url'           => true,
            'type'      => 'media',
            'title'     => esc_html__( 'Favicon', 'coaf' ),
            'subtitle'  => esc_html__( 'Upload your custom site favicon.', 'coaf' ),
        ),

        array(
            'id'        => 'iphone_icon',
            'url'       => true,
            'type'      => 'media',
            'title'     => esc_html__( 'Apple iPhone Icon ', 'coaf' ),
            'subtitle'  => esc_html__( 'Upload your custom iPhone icon (57px by 57px).', 'coaf' ),
        ),

        array(
            'id'        => 'iphone_icon_retina',
            'url'       => true,
            'type'      => 'media',
            'title'     => esc_html__( 'Apple iPhone Retina Icon ', 'coaf' ),
            'subtitle'  => esc_html__( 'Upload your custom iPhone retina icon (114px by 114px).', 'coaf' ),
        ),

        array(
            'id'        => 'ipad_icon',
            'url'       => true,
            'type'      => 'media',
            'title'     => esc_html__( 'Apple iPad Icon ', 'coaf' ),
            'subtitle'  => esc_html__( 'Upload your custom iPad icon (72px by 72px).', 'coaf' ),
        ),

        array(
            'id'        => 'ipad_icon_retina',
            'url'       => true,
            'type'      => 'media',
            'title'     => esc_html__( 'Apple iPad Retina Icon ', 'coaf' ),
            'subtitle'  => esc_html__( 'Upload your custom iPad retina icon (144px by 144px).', 'coaf' ),
        ),
    )
));

/*--------------------------------------------------------*/
/* Header
/*--------------------------------------------------------*/

Redux::setSection( $opt_name, array(
    'title'      => esc_html__( 'Header', 'coaf' ),
    'id'         => 'header_options',
    'subsection' => false,
));

/**
 * header Section
 */
Redux::setSection( $opt_name, array(
    'title'   => esc_html__( 'Header options', 'coaf' ),
    'id'      => 'headers_options',
    'subsection' => true,
    'fields'  => array(
        // Enable Search
        array(
            'title'            => esc_html__('Enable Search', 'coaf'),
            'id'               => 'header_search',
            'type'             => 'switch',
            'default'          => true,
        ),
        // Enable Donate
        array(
            'title'            => esc_html__('Enable Donate', 'coaf'),
            'id'               => 'header_donate',
            'type'             => 'switch',
            'default'          => true,
        ),
        array(
            'title'            => esc_html__('Donate URL', 'coaf'),
            'id'               => 'header_donate_url',
            'type'             => 'text',
            'default'          => esc_html( '#' ),
            'required'         => array('header_donate', '=', true),
        ),
        // Enable Cart
        array(
            'title'            => esc_html__('Enable header cart', 'coaf'),
            'id'               => 'header_cart',
            'type'             => 'switch',
            'default'          => true,
        ),
        // Enable Language switcher
        array(
            'title'            => esc_html__('Enable Language switcher', 'coaf'),
            'id'               => 'header_lang',
            'type'             => 'switch',
            'default'          => true,
        ),
    ),

));
/*--------------------------------------------------------*/
/* Footer
/*--------------------------------------------------------*/

Redux::setSection( $opt_name, array(
    'title'      => esc_html__( 'Footer  options', 'coaf' ),
    'id'         => 'footer_options',
    'subsection' => false,
));

/**
 * Footer Section
 */
Redux::setSection( $opt_name, array(
    'title'   => esc_html__( 'Footer options', 'coaf' ),
    'id'      => 'footers_options',
    'subsection' => true,
    'fields'  => array(
        array(
            'id'        => 'footer_logo',
            'url'       => true,
            'type'      => 'media',
            'title'     => esc_html__( 'Footer Logo', 'coaf' ),
            'read-only' => false,
            'default'   => array( 'url' => get_template_directory_uri() . '/assets/imgs/footer-logo.svg' ),
            'subtitle'  => esc_html__( 'Upload your custom site logo.', 'coaf' ),
        ),
        array(
            'title'            => esc_html__('Logo Margin', 'coaf'),
            'subtitle'         => esc_html__('Move your logo around to fit it perfectly.', 'coaf'),
            'id'               => 'footer_logo_margin',
            'type'             => 'spacing',
            'output'           => array('.footer-bottom__logo'),
            'mode'             => 'margin',
            'units'            => array('px'),
            'units_extended'   => 'false',
            'default'          => array(
                'margin-top'     => '0px',
                'margin-right'   => '0px',
                'margin-bottom'  => '0px',
                'margin-left'    => '0px',
                'units'          => 'px',
            )
        ),
        // Enable Scroll top
        array(
            'title'            => esc_html__('Enable Scroll Top', 'coaf'),
            'id'               => 'scroll_top',
            'type'             => 'switch',
            'default'          => true,
        ),

    ),

));

/**
 * Copyright Section
 */
Redux::setSection( $opt_name, array(
    'title'   => esc_html__( 'Copyright', 'coaf' ),
    'id'      => 'copyright_options',
    'subsection' => true,
    'fields'  => array(

        // Top footer area
        array(
            'title'            => esc_html__('Copyright Text', 'coaf'),
            'subtitle'         => esc_html__('Enter the text which you would like in the copyright section (HTML Allowed).', 'coaf'),
            'id'               => 'copyright_text',
            'type'             => 'textarea',
            'default'          => esc_html( '© 2005-2021 Children of Armenia Charitable Fund. All rights reserved.' )
        ),
        // Top footer area
        array(
            'title'            => esc_html__('Theme Creator', 'coaf'),
            'subtitle'         => esc_html__('Enter the text Theme Creator.', 'coaf'),
            'id'               => 'creater_text',
            'type'             => 'textarea',
            'default'          => esc_html( 'VECTO' )
        ),
        // Enable Facebook
        array(
            'title'            => esc_html__('Enable Facebook', 'coaf'),
            'id'               => 'footer_social_facebook',
            'type'             => 'switch',
            'default'          => true,
        ),

        // Facebook URL
        array(
            'title'            => esc_html__('Facebook URL', 'coaf'),
            'id'               => 'footer_social_facebook_url',
            'type'             => 'text',
            'default'          => esc_html( 'http://www.facebook.com' ),
            'required'         => array('footer_social_facebook', '=', true),
        ),

        // Enable Instagram
        array(
            'title'            => esc_html__('Enable Instagram', 'coaf'),
            'id'               => 'footer_social_instagram',
            'type'             => 'switch',
            'default'          => true,
        ),

        // Instagram URL
        array(
            'title'            => esc_html__('Instagram URL', 'coaf'),
            'id'               => 'footer_social_instagram_url',
            'type'             => 'text',
            'default'          => esc_html( 'https://www.instagram.com/' ),
            'required'         => array('footer_social_instagram', '=', true),
        ),

        // Enable Twitter
        array(
            'title'            => esc_html__('Enable Twitter', 'coaf'),
            'id'               => 'footer_social_twitter',
            'type'             => 'switch',
            'default'          => true,
        ),

        // Twitter URL
        array(
            'title'            => esc_html__('Twitter URL', 'coaf'),
            'id'               => 'footer_social_twitter_url',
            'type'             => 'text',
            'default'          => esc_html( 'http://www.twitter.com' ),
            'required'         => array('footer_social_twitter', '=', true),
        ),

        // Enable Linkdin
        array(
            'title'            => esc_html__('Enable Linkdin', 'coaf'),
            'id'               => 'footer_social_linkedin',
            'type'             => 'switch',
            'default'          => true,
        ),

        // Linkdin URL
        array(
            'title'            => esc_html__('Linkdin URL', 'coaf'),
            'id'               => 'footer_social_twitter_url',
            'type'             => 'text',
            'default'          => esc_html( 'https://www.linkedin.com/' ),
            'required'         => array('footer_social_linkedin', '=', true),
        ),

        // Enable Youtube
        array(
            'title'            => esc_html__('Enable youtube', 'coaf'),
            'id'               => 'footer_social_youtube',
            'type'             => 'switch',
            'default'          => false,
        ),

        // Youtube URL
        array(
            'title'            => esc_html__('Youtube URL', 'coaf'),
            'id'               => 'footer_social_youtube_url',
            'type'             => 'text',
            'default'          => esc_html( 'https://www.youtube.com/' ),
            'required'         => array('footer_social_youtube', '=', true),
        ),

        // Enable Pinterest
        array(
            'title'            => esc_html__('Enable Pinterest', 'coaf'),
            'id'               => 'footer_social_pinterest',
            'type'             => 'switch',
            'default'          => false,
        ),

        // Pinterest URL
        array(
            'title'            => esc_html__('Pinterest URL', 'coaf'),
            'id'               => 'footer_social_pinterest_url',
            'type'             => 'text',
            'default'          => esc_html( 'https://www.pinterest.com/' ),
            'required'         => array('footer_social_pinterest', '=', true),
        ),
        // Enable Medium
        array(
            'title'            => esc_html__('Enable Medium', 'coaf'),
            'id'               => 'footer_social_medium',
            'type'             => 'switch',
            'default'          => false,
        ),

        // Medium URL
        array(
            'title'            => esc_html__('Medium URL', 'coaf'),
            'id'               => 'footer_social_medium_url',
            'type'             => 'text',
            'default'          => esc_html( 'https://medium.com/' ),
            'required'         => array('footer_social_medium', '=', true),
        ),

        // Enable Vimeo
        array(
            'title'            => esc_html__('Enable Vimeo', 'coaf'),
            'id'               => 'footer_social_vimeo',
            'type'             => 'switch',
            'default'          => false,
        ),

        // Vimeo URL
        array(
            'title'            => esc_html__('Vimeo URL', 'coaf'),
            'id'               => 'footer_social_vimeo_url',
            'type'             => 'text',
            'default'          => esc_html( 'https://vimeo.com/' ),
            'required'         => array('footer_social_vimeo', '=', true),
        ),
    ),

));