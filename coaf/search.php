<?php get_header(); ?>

<?php printf(__( 'Search Results for: %s', 'coaf' ), get_search_query() ); ?>

<?php while ( have_posts() ) : the_post(); ?>
    <?php get_template_part( 'content', 'archive' ); ?>
<?php endwhile; ?>

<?php get_footer(); ?>