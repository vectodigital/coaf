  <footer class="footer">
    <?php 
      $footer_logo = coaf_option( 'footer_logo', false, 'url' );
      $scroll_top = coaf_option( 'scroll_top', false );  
    ?>
    <div class="footer__bottom footer-bottom">  
      <div class="container">
        <div class="row">
          <div class="col-md-4">
            <?php if(!empty($footer_logo)){ ?>
              <a class="footer-bottom__logo" href="<?php echo home_url(); ?>"><img src="<?php echo esc_url( $footer_logo ); ?>" alt="<?php bloginfo( 'Name' ); ?>"></a>
            <?php } ?>
              
              <div class="footer-bottom__socials">
                <?php get_template_part( 'templates/social-links' ); ?>
              </div>
            </div>
        <div class="col-md-4">
          <?php if ( is_active_sidebar( 'footer-col-1' ) ) : ?>
            <div class="footer-bottom__quick">
              <?php dynamic_sidebar('footer-col-1'); ?>
            </div>
          <?php endif; ?>
        </div>
        <div class="col-md-4">
          <?php if ( is_active_sidebar( 'footer-col-2' ) ) : ?>
            <div class="footer-bottom__separator">
              <?php dynamic_sidebar('footer-col-2'); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
        <div class="footer-bottom__copyright">
          <?php get_template_part( 'template-parts/footer/copyrights' ) ?>
        </div>
      </div>
    </div>
  </footer>
  <?php if($scroll_top == true){ ?>
            <a class="footer-bottom__totop" href="#main">
              <svg xmlns="http://www.w3.org/2000/svg" width="46" height="46" viewBox="0 0 46 46"><g transform="translate(-1292 -5815)"><rect width="46" height="46" rx="23" transform="translate(1292 5815)"/><g transform="translate(1 -2)"><g transform="translate(1322.026 5892.81) rotate(180)"><g transform="translate(0 48.907)"><path class="totop-arrow" d="M14.136,48.907,8.013,55.03,1.89,48.907,0,50.8,8.013,58.81,16.026,50.8Z" transform="translate(0 -48.907)"/></g></g><rect width="18" height="18" transform="translate(1305 5830)" fill="none"/></g></g></svg>
            </a>
          <?php } ?>
  <?php wp_footer(); ?>
</body>
</html>