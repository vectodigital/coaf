<?php 
/*  Template Name: Front page */
    get_header( );
?>
  <main class="main" id="main">

    <?php the_title('<h1 class="main__title">', '</h1>'); ?>
   
    <?php the_content(); ?>

  </main>

<?php 
    get_footer( );
?>