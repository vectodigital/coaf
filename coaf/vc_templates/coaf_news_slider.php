<?php
extract(shortcode_atts(array(
    'news_block_type' => '',
    'bloggrid_block_category' => '',
    'news_number' => '',
    'news_image_size' => '',
    'news_block_description_lenght' => '',
    'news_block_link_url' => '',
    'news_block_link_text' => '',

), $atts ));

$query = new WP_Query(array(
    'post_type' => $news_block_type,
    'category__in' => $bloggrid_block_category,
    'posts_per_page' => esc_attr( $news_number ),
));

$val = randomik();
?>
<div id="<?php echo $val; ?>" class="main__stories">
      <div>
        <h2 class="main__stories-title title">Stories</h2>
        <div class="main__stories-arrows">
          <button class="main__stories-arrow main__stories-prev" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 52 52"><g class="arrow-bg" transform="translate(50 50) rotate(180) scale(.9)"><rect width="52" height="52" rx="26" stroke="none"></rect><rect x="0.5" y="0.5" width="51" height="51" rx="25.5" fill="none"></rect></g><g transform="translate(15 18)"><g transform="translate(20.999 0.999) rotate(90)"><path d="M14.693,12.96,7.7,19.953a.413.413,0,0,1-.584,0L.121,12.96a.413.413,0,0,1,0-.584l1.863-1.864a.412.412,0,0,1,.584,0l3,3V.413A.413.413,0,0,1,5.979,0H8.834a.413.413,0,0,1,.413.413v13.1l3-3a.425.425,0,0,1,.584,0l1.864,1.864a.413.413,0,0,1,0,.584Z"></path></g><rect width="22" height="16" fill="none"></rect></g></svg>
          </button>
          <button class="main__stories-arrow main__stories-next" type="button">
            <svg xmlns="http://www.w3.org/2000/svg" width="52" height="52" viewBox="0 0 52 52"><g class="arrow-bg" transform="translate(50 50) rotate(180) scale(.9)"><rect class="arrow-bg" width="52" height="52" rx="26" stroke="none"></rect><rect x="0.5" y="0.5" width="51" height="51" rx="25.5" fill="none"></rect></g><g transform="translate(15 18)"><g transform="translate(2 15) rotate(270)"><path d="M14.693,12.96,7.7,19.953a.413.413,0,0,1-.584,0L.121,12.96a.413.413,0,0,1,0-.584l1.863-1.864a.412.412,0,0,1,.584,0l3,3V.413A.413.413,0,0,1,5.979,0H8.834a.413.413,0,0,1,.413.413v13.1l3-3a.425.425,0,0,1,.584,0l1.864,1.864a.413.413,0,0,1,0,.584Z"></path></g><rect width="22" height="16" fill="none"></rect></g></svg>
          </button>
        </div>
      </div>
      <div class="main__stories-items invisible-scroll">   
   <?php if( $query->have_posts() ): while( $query->have_posts() ): $query->the_post(); ?>

   <div class="main__stories-item">
          <div class="main__stories-iteminner">
            <?php the_post_thumbnail( 'coaf-image-303x394-cropped', array('class' => 'main__stories-childpic') ); ?>
            <div class="main__stories-info">
              <p class="main__stories-date"><?php the_date(); ?></p>
              <p class="main__stories-name"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></p>
              <p class="main__stories-descr"><?php the_excerpt(); ?></p>
            </div>
          </div>
        </div>
   <?php endwhile; wp_reset_postdata(); endif; ?>
</div>    
  <?php if(!empty($news_block_link_url)){ ?>
    <div class="main__stories-morewrap">
      <a href="<?php echo $news_block_link_url; ?>" class="main__stories-more link-more"><?php echo $news_block_link_text ?></a>
    </div>
    <?php } ?>
  </div>