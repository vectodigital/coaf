<?php

extract(shortcode_atts(array(
    'map_image' => '',
    'map_width' => '',
    'map_height' => '',
    'values' => '',

), $atts));
$val = randomik();

// vars
$featured_img = wp_get_attachment_image( $map_image, 'full' );
  $values = vc_param_group_parse_atts($atts['values']);

  $new_accordion_value = array();
  foreach($values as $data){
    $new_line = $data;
    $new_line['marker_title'] = isset($new_line['marker_title']) ? $new_line['marker_title'] : '';
    $new_line['marker_description'] = isset($new_line['marker_description']) ? $new_line['marker_description'] : '';
    $new_line['marker_top'] = isset($new_line['marker_top']) ? $new_line['marker_top'] : '';
    $new_line['marker_left'] = isset($new_line['marker_left']) ? $new_line['marker_left'] : '';
    $new_line['marker_bottom'] = isset($new_line['marker_bottom']) ? $new_line['marker_bottom'] : '';
    $new_line['marker_right'] = isset($new_line['marker_right']) ? $new_line['marker_right'] : '';
    $new_line['marker_position'] = isset($new_line['marker_position']) ? $new_line['marker_position'] : '';
    $new_line['marker_orientation'] = isset($new_line['marker_orientation']) ? $new_line['marker_orientation'] : '';
    $new_line['marker_color'] = isset($new_line['marker_color']) ? $new_line['marker_color'] : '';
    $new_line['marker_image'] = isset($new_line['marker_image']) ? $new_line['marker_image'] : '';

    $new_accordion_value[] = $new_line;

  }

?>
<div class="main__location">
      <div class="main__location-img">
	    <?php echo wp_kses_post( $featured_img ) ?>
      </div>
	  <?php foreach ($new_accordion_value as $value) { ?>
      <div id="<?php echo $val; ?>" class="main__location-location">
		<?php
		if(!empty($value['marker_image'])) {
				$image = wp_get_attachment_image( $value['marker_image'], 'thumbnail', true );
				echo $image;
			} 
		?>
        <p class="main__location-name"><?php echo $value['marker_title']; ?></p>
      </div>
	  <?php } ?>
    </div>
<style>
<?php foreach ($new_accordion_value as $value) { ?>
	<?php echo '#'.$val; ?> {
			left: <?php echo $value['marker_left']; ?>;
			top: <?php echo $value['marker_top']; ?>;
			right: <?php echo $value['marker_right']; ?>;
			bottom: <?php echo $value['marker_bottom']; ?>;
	}
<?php } ?>
</style>
