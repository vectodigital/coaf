<?php
extract(shortcode_atts(array(
    'values' => '',

), $atts ));

  $values = vc_param_group_parse_atts($atts['values']);

  $new_counter = array();
  foreach($values as $data){
    $new_line = $data;
    $new_line['counter_title'] = isset($new_line['counter_title']) ? $new_line['counter_title'] : '';
    $new_line['counter_number'] = isset($new_line['counter_number']) ? $new_line['counter_number'] : '';
    $new_line['counter_symbol'] = isset($new_line['counter_symbol']) ? $new_line['counter_symbol'] : '';
    $new_line['counter_icon'] = isset($new_line['counter_icon']) ? $new_line['counter_icon'] : '';
    $new_line['counter_increment'] = isset($new_line['counter_increment']) ? $new_line['counter_increment'] : '';

    $new_counter[] = $new_line;

  }

?>
<ul class="main__what-list">
<?php foreach ($new_counter as $value) { 
  $image  = wp_get_attachment_image_url($value['counter_icon'], 'full');
  ?>
    <li class="main__what-item beneficiaries">
    <div class="main__icon-info">
        <?php if(!empty($value['counter_icon'])){ ?>
          <img src="<?php echo $image; ?>" alt="<?php echo $value['counter_title']; ?>">
        <?php }  ?>
    </div>
      <div class="main__what-info">
        <p class="main__what-num main-black">
           <span class='numscroller' data-min='1' data-max='<?php echo $value['counter_number']; ?>' data-delay='5' data-increment='<?php echo $value['counter_increment']; ?>'><?php echo $value['counter_number']; ?></span>
         <?php echo $value['counter_symbol'] ?>
        </p>
        <p class="main__what-inftext">
            <?php echo $value['counter_title']; ?>
        </p>
      </div>
</li>
<?php } ?>
</ul>