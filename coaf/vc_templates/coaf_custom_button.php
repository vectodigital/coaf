<?php

extract(shortcode_atts(array(

    // general
    'custom_button_text' => '',
    'custom_button_link' => '',
    'custom_button_target' => '',
    'custom_button_size' => '',
    'custom_button_corners' => '',

    // button position
    'custom_button_alignment' => '',

    // button colors
    'button_background_color' => '',
    'button_text_color' => '',
    'button_background_color_hov' => '',
    'button_text_color_hov' => '',

    // button icon
    'button_icon' => '',
    'button_icon_color' => '',
    'button_icon_color_hov' => '',

), $atts));


// css classes
$button_classes = esc_attr( $custom_button_size ) . ' ' . esc_attr( $custom_button_corners );
$button_classes_wrapper = esc_attr( $custom_button_alignment );


// Unique ID for each button
$button_id = uniqid( 'CustomButton_' );
$button_id_css = '#' . esc_attr( $button_id );

?>

<style>

    <?php // background color
    echo $button_id_css ?> .custom-button {
        background: <?php echo esc_attr( $button_background_color ) ?>;
    }

    <?php // text color
    echo $button_id_css ?> .custom-button a {
       color: <?php echo esc_attr( $button_text_color ) ?>;
    }

    <?php // background hover color
    echo $button_id_css ?> .custom-button:hover {
       background: <?php echo esc_attr( $button_background_color_hov ) ?>;
    }

    <?php // text hover color
    echo $button_id_css ?> .custom-button:hover a {
       color: <?php echo esc_attr( $button_text_color_hov ) ?>;
    }

    <?php // icon color
    echo $button_id_css ?> .custom-button i {
       color: <?php echo esc_attr( $button_icon_color ) ?>;
    }

    <?php // icon hover color
    echo $button_id_css ?> .custom-button:hover i {
       color: <?php echo esc_attr( $button_icon_color_hov ) ?>;
    }

</style>


<div class="custom-button-wrapper <?php echo esc_attr( $button_classes_wrapper ) ?>" id="<?php echo esc_attr( $button_id ) ?>">
    <div class="custom-button <?php echo esc_attr( $button_classes ) ?>">
        <a class="popup-item" href="<?php echo esc_attr( $custom_button_link ); ?>" target="<?php echo esc_attr( $custom_button_target ); ?>"><?php echo esc_attr( $custom_button_text ); ?></a>
        <?php if( !empty( $button_icon ) ) { ?>
            <i class="<?php echo esc_attr( $button_icon ); ?>"></i>
        <?php } ?>
    </div>
</div>