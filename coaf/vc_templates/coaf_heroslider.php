<?php extract(shortcode_atts(array(
    'coaf_slider_slider' => '',
), $atts));

$sliders = vc_param_group_parse_atts($coaf_slider_slider);
?>
<div class="main__hero-wrapper">
      <div class="main__hero">
        <?php 
            foreach($sliders as $slider){
              if(!empty($slider['coaf_slider_title'])) {
                  echo '<div class="main__hero-item">';
                  $image  = wp_get_attachment_image_url($slider['coaf_slider_img'], 'full');
                  echo '<img class="main__hero-img" src="'.$image.'" alt="'.$slider['coaf_slider_title'].'">';
                  echo '<div class="main__hero-content container">
                        <h2 class="main__hero-title title">'.$slider['coaf_slider_title'].'</h2>
                        <p class="main__hero-descr">'.$slider['coaf_slider_desc'].'</p>
                    </div>';
                  echo '</div>';
              }
            }
        ?>
      </div>
      <?php 
            foreach($sliders as $slider){
              if(!empty($slider['coaf_slider_title'])) {
                  echo '<div class="main__hero-names container">';
                  echo '<p class="main__hero-name">'.$slider['coaf_slider_title'].'</p>';
                  echo '</div>';
              }
            }
        ?>
    </div>