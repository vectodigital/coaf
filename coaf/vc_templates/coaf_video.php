<?php
extract(shortcode_atts(array(
    'video_link' => '',
    'video_file' => '',
    'video_poster' => '',
    'video_height' => '',
), $atts));

$vidweo_link = wp_get_attachment_url($video_file);
$vidweo_thumb = wp_get_attachment_url( $video_poster , 'coaf-image-1162x442-cropped');
$number = rand(5, 15);

?>

<div class="main__villages-vidwrapper">
    <?php if(!empty($video_file)){ ?>
        <video class="main__villages-video" src="<?php echo $video_file; ?>" poster="<?php echo $vidweo_thumb; ?>"></video>
    <?php }else{ ?>
       
        <img src="<?php echo $vidweo_thumb; ?>">
        <iframe id="videovc" width="100%" height="<?php echo $video_height; ?>px" src="<?php echo $video_link; ?>" frameborder="0" allowfullscreen class="embed-responsive-item" data-play="0"></iframe>
        <button class="main__villages-vidplay" type="button"></button>
       <script>
jQuery('.main__villages-vidplay').click(function(){
    var videoURL = jQuery('#videovc').attr('src'),
    dataplay = jQuery('#videovc').attr('data-play');

    //for check autoplay
    //if not set autoplay=1
    if(dataplay == 0 ){
        jQuery('#videovc').attr('src',videoURL+'?autoplay=1');
        jQuery('#videovc').attr('data-play',1);
        jQuery('.main__villages-vidwrapper').addClass('active');
     }else{
        var videoURL = jQuery('#videovc').attr('src');
        videoURL = videoURL.replace("?autoplay=1", "");
        jQuery('#videovc').prop('src','');
        jQuery('#videovc').prop('src',videoURL);
        jQuery('.main__villages-vidwrapper').removeClass('active');
        jQuery('#videovc').attr('data-play',0);
     }

});
        </script>
    <?php } ?>
        
</div>


