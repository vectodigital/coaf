<form role="search" method="get" id="searchform" action="<?php echo home_url( '/' ) ?>" >
<button class="header__search-btn" type="button">
    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22">
        <g transform="translate(-1092 -25)"><path d="M12.819,17.207C1.406,16.734,1.408.471,12.819,0a8.6,8.6,0,1,1,0,17.207Zm0-15.576A6.98,6.98,0,0,0,5.847,8.6c.387,9.256,13.567,9.246,13.945-.016A6.972,6.972,0,0,0,12.819,1.631ZM1.361,20.917a.815.815,0,0,1-.575-1.394l3.67-3.649a.815.815,0,1,1,1.15,1.156L1.936,20.68A.813.813,0,0,1,1.361,20.917Z" transform="translate(1092.239 25.786)"/>
        <rect width="22" height="22" transform="translate(1092 25)" fill="none"/>
        </g>
    </svg>
</button>
<input class="header__search-input" value="<?php echo get_search_query() ?>" type="text" name="s">
</form>
            