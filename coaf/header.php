<?php
/**
 * @package Coaf
 * @since Coaf 1.0
 */
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<?php 
    global $coaf_options; 
    $custom_logo = coaf_option( 'custom_logo', false, 'url' ); 
    $lang = coaf_option( 'header_lang', false ); 
    $donate = coaf_option( 'header_donate', false ); 
    $search = coaf_option( 'header_search', false ); 
    $cart = coaf_option( 'header_cart', false ); 
    $header_donate_url = coaf_option( 'header_donate_url', false ); 
    ?>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  <?php wp_body_open(); ?>
  <header class="header">
    <div class="header__container container">
    <?php if( !empty( $custom_logo ) ) { ?>
      <a class="header__logo" href="<?php echo home_url(); ?>">
        <img src="<?php echo esc_url( $custom_logo ) ?>" alt="<?php bloginfo( 'Name' ) ?>">
      </a>
    <?php } ?>
      <div class="header__content">
      <nav class="navbar navbar-expand-md justify-content-end">
          <!-- menu -->
          <div class="main-menu align-items-center">
              <?php wp_nav_menu( array(
                  'theme_location' => 'menu-1',
                  'container'      => false,
                  'fallback_cb'    => true,
                  'depth'          => 3,
                  'menu_class'     => 'header__nav-list nav-list'
              )
              ); ?>
          </div>
      </nav> 
        <?php if($donate == true){ ?>
          <a class="header__donate btn-primary" href="<?php echo $header_donate_url; ?>"><?php esc_html_e('Donate', 'coaf') ?></a>
        <?php } ?>
              
        <div class="header__right">
        <?php if($donate == true){ ?>
          <div class="header__search">
            <?php get_search_form( ); ?>
          </div>
          <?php } ?>
          <?php if($donate == true){ ?>
          <a class="header__calendar" href="#">
            calendar
            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22"><g transform="translate(-1197 -30)"><g transform="translate(2638.211 -382.32)"><path d="M-1431.842,415.81h3.275c.043-.225.071-.444.129-.655a2.5,2.5,0,0,1,2.63-1.825,2.476,2.476,0,0,1,2.255,2.264.374.374,0,0,0,.324.393,4.129,4.129,0,0,1,3.031,4q.018,4.592,0,9.183a4.133,4.133,0,0,1-4.178,4.164q-5.823.021-11.645,0a4.144,4.144,0,0,1-4.181-4.2q-.018-4.552,0-9.105a4.138,4.138,0,0,1,2.995-4.027.428.428,0,0,0,.365-.445,2.476,2.476,0,0,1,2.373-2.226,2.492,2.492,0,0,1,2.56,2.064C-1431.888,415.516-1431.869,415.645-1431.842,415.81Zm-6.7,7.535v.394c0,1.757,0,3.515,0,5.272a2.516,2.516,0,0,0,2.679,2.659q5.643,0,11.287,0a2.511,2.511,0,0,0,2.714-2.7c.006-1.744,0-3.489,0-5.233,0-.126-.011-.253-.018-.391Zm15.023-5.694a2.569,2.569,0,0,1-2.46,2.358c-1.314.044-2.132-.737-2.609-2.5h-3.221c-.441,1.711-1.208,2.48-2.5,2.5a2.451,2.451,0,0,1-1.5-.467,2.6,2.6,0,0,1-1.06-1.863,2.3,2.3,0,0,0-1.435,1.275,7.229,7.229,0,0,0-.236,2.679h16.629a15.208,15.208,0,0,0-.02-2.127A2.314,2.314,0,0,0-1423.519,417.651Zm-10.026-.979c0-.286.008-.573,0-.859a.824.824,0,0,0-.817-.823.814.814,0,0,0-.828.774c-.022.6-.023,1.2,0,1.8a.807.807,0,0,0,.835.763.815.815,0,0,0,.806-.792C-1433.533,417.245-1433.546,416.958-1433.546,416.672Zm6.691-.022c0,.286-.01.573,0,.859a.817.817,0,0,0,.824.814.823.823,0,0,0,.826-.817c.016-.572.018-1.146,0-1.718a.825.825,0,0,0-.848-.8.819.819,0,0,0-.8.8C-1426.865,416.076-1426.854,416.363-1426.854,416.65Z" transform="translate(0 0)" /><path d="M-1321.172,563.49a.863.863,0,0,1-.835.822.864.864,0,0,1-.841-.856.857.857,0,0,1,.835-.821A.863.863,0,0,1-1321.172,563.49Z" transform="translate(-108.188 -137.642)"/><path d="M-1269.363,563.479a.834.834,0,0,1,.8-.842.839.839,0,0,1,.855.83.842.842,0,0,1-.845.843A.832.832,0,0,1-1269.363,563.479Z" transform="translate(-157.492 -137.643)"/><path d="M-1374.453,606.185a.812.812,0,0,1-.832.84.824.824,0,0,1-.825-.813.835.835,0,0,1,.836-.845A.822.822,0,0,1-1374.453,606.185Z" transform="translate(-59.09 -177.035)"/><path d="M-1268.525,607.028a.813.813,0,0,1-.838-.8.822.822,0,0,1,.812-.864.839.839,0,0,1,.845.8A.831.831,0,0,1-1268.525,607.028Z" transform="translate(-157.492 -177.036)" /><path d="M-1374.456,563.456a.837.837,0,0,1-.832.851.843.843,0,0,1-.824-.824.841.841,0,0,1,.836-.85A.836.836,0,0,1-1374.456,563.456Z" transform="translate(-59.088 -137.64)" /><path d="M-1322,607.027a.84.84,0,0,1-.854-.832.842.842,0,0,1,.822-.826.844.844,0,0,1,.853.836A.837.837,0,0,1-1322,607.027Z" transform="translate(-108.187 -177.035)" /></g><rect width="22" height="22" transform="translate(1197 30)" fill="none"/></g></svg>
          </a>
          <?php } ?>
          <?php if($donate == true){ ?>
          <div class="header__langs">
            <ul class="header__langs-list">
              <li class="header__langs-item header__langs-current">
                <button class="header__langs-currbtn en" type="button">En</button>
                <ul class="header__langs-sub">
                  <li class="header__langs-item current">
                    <span class="en">En</span>
                  </li>              
                  <li class="header__langs-item">
                    <a class="arm" href="#">Arm</a>
                  </li>              
                </ul>
              </li>
            </ul>
          </div>
          <?php } ?>
        </div>
        <?php get_template_part('template-parts/header/mobile-menu'); ?> 
      </div>
    </div>
  </header>