<div class="footer-bottom__social-wrapper">
            <ul class="footer-bottom__socgroup">
              <li class="footer-bottom__soc-item">
                <a class="footer-bottom__soc-link social-link" href="#" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                    <g transform="translate(-493 -5629)">
                      <g class="soc-ellipse" transform="translate(493 5629)">
                        <circle cx="18" cy="18" r="18" stroke="none"/>
                        <circle cx="18" cy="18" r="17.5"/>
                      </g>
                      <g class="inner-icon" transform="translate(-1.419 4.581)">
                        <g transform="translate(508.058 5634.418)">
                          <g transform="translate(0)">
                            <path d="M133,5.5v-2a1,1,0,0,1,1-1h1V0h-2a3,3,0,0,0-3,3V5.5h-2V8h2v8h3V8h2l1-2.5Z" transform="translate(-128)"/>
                          </g>
                        </g>
                        <rect width="18" height="18" transform="translate(503.419 5633.419)" fill="none"/>
                      </g>
                    </g>
                  </svg>
                </a>
              </li>
              <li class="footer-bottom__soc-item">
                <a class="footer-bottom__soc-link social-link" href="#" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                    <g transform="translate(-554 -5626)">
                      <g class="soc-ellipse" transform="translate(554 5626)">
                        <circle cx="18" cy="18" r="18" stroke="none"/>
                        <circle cx="18" cy="18" r="17.5"/>
                      </g>
                      <g class="inner-icon" transform="translate(1.459 5.459)">
                        <g transform="translate(562.472 5630.472)">
                              <path d="M11.1,0H5.045A5.045,5.045,0,0,0,0,5.045V11.1a5.045,5.045,0,0,0,5.045,5.045H11.1A5.045,5.045,0,0,0,16.144,11.1V5.045A5.045,5.045,0,0,0,11.1,0ZM14.63,11.1A3.535,3.535,0,0,1,11.1,14.63H5.045A3.535,3.535,0,0,1,1.513,11.1V5.045A3.535,3.535,0,0,1,5.045,1.513H11.1A3.535,3.535,0,0,1,14.63,5.045Z"/>
                          <g transform="translate(4.036 4.036)">
                              <path d="M132.036,128a4.036,4.036,0,1,0,4.036,4.036A4.036,4.036,0,0,0,132.036,128Zm0,6.558a2.522,2.522,0,1,1,2.522-2.522A2.526,2.526,0,0,1,132.036,134.558Z" transform="translate(-128 -128)"/>
                          </g>
                          <g transform="translate(11.873 3.195)">
                              <circle cx="0.538" cy="0.538" r="0.538"/>
                          </g>
                        </g>
                        <rect width="18" height="18" transform="translate(561.541 5629.541)" fill="none"/>
                      </g>
                    </g>
                  </svg>
                  
                </a>
              </li>
              <li class="footer-bottom__soc-item">
                <a class="footer-bottom__soc-link social-link" href="#" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                    <g transform="translate(-612 -5626)">
                      <g class="soc-ellipse" transform="translate(612 5626)">
                        <circle cx="18" cy="18" r="17.5"/>
                      </g>
                      <g class="inner-icon" transform="translate(0.002 3.002)">
                        <g transform="translate(621.701 5634.084)">
                          <g transform="translate(0 0)">
                            <path d="M16.848,49.621a7.2,7.2,0,0,1-1.99.545,3.435,3.435,0,0,0,1.52-1.909,6.9,6.9,0,0,1-2.19.836,3.454,3.454,0,0,0-5.975,2.362,3.557,3.557,0,0,0,.08.788,9.777,9.777,0,0,1-7.12-3.613,3.455,3.455,0,0,0,1.061,4.616,3.411,3.411,0,0,1-1.561-.425v.038a3.47,3.47,0,0,0,2.767,3.394,3.447,3.447,0,0,1-.906.114,3.054,3.054,0,0,1-.654-.059,3.487,3.487,0,0,0,3.228,2.406A6.94,6.94,0,0,1,.827,60.187,6.469,6.469,0,0,1,0,60.139a9.724,9.724,0,0,0,5.3,1.55A9.763,9.763,0,0,0,15.13,51.86c0-.153-.005-.3-.013-.446A6.891,6.891,0,0,0,16.848,49.621Z" transform="translate(0 -48)"/>
                          </g>
                        </g>
                        <rect width="18" height="18" transform="translate(620.998 5631.998)" fill="none"/>
                      </g>
                    </g>
                  </svg>            
                </a>
              </li>
              <li class="footer-bottom__soc-item">
                <a class="footer-bottom__soc-link social-link" href="#" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                    <g transform="translate(-670 -5626)">
                      <g class="soc-ellipse" transform="translate(670 5626)">
                        <circle cx="18" cy="18" r="18" stroke="none"/>
                        <circle cx="18" cy="18" r="17.5"/>
                      </g>
                      <g class="inner-icon" transform="translate(1.842 1.842)">
                        <g transform="translate(679.134 5635.134)">
                          <g transform="translate(0.12 4.392)">
                              <rect class="wt" width="3.057" height="9.399" />
                          </g>
                          <g transform="translate(4.392 4.392)">
                              <path d="M167.113,160.11c-.032-.01-.063-.021-.1-.031s-.082-.017-.124-.024a2.74,2.74,0,0,0-.549-.056,4.248,4.248,0,0,0-3.285,1.8V160H160v9.4h3.057v-5.127s2.311-3.218,3.285-.854V169.4H169.4v-6.343A3.048,3.048,0,0,0,167.113,160.11Z" transform="translate(-160 -160)"/>
                          </g>
                          <g transform="translate(0 0)">
                              <circle class="wt" cx="1.526" cy="1.526" r="1.526" />
                          </g>
                        </g>
                        <rect width="18" height="18" transform="translate(677.158 5633.158)" fill="none"/>
                      </g>
                    </g>
                  </svg>
                </a>
              </li>
              <li class="footer-bottom__soc-item">
                <a class="footer-bottom__soc-link social-link" href="#" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                    <g transform="translate(-728 -5626)">
                      <g class="soc-ellipse" transform="translate(728 5626)">
                        <circle cx="18" cy="18" r="18" stroke="none"/>
                        <circle cx="18" cy="18" r="17.5"/>
                      </g>
                      <g class="inner-icon" transform="translate(3.154 5.154)">
                        <g transform="translate(735.344 5633.39)">
                          <g transform="translate(0 0)">
                            <path d="M14.329,80.991c-.406-.722-.846-.855-1.743-.905S9.437,80,7.485,80s-4.21.025-5.105.085-1.337.183-1.746.906S0,82.955,0,85.142v.007c0,2.177.215,3.429.633,4.143s.85.853,1.745.914,3.15.083,5.106.083,4.205-.031,5.1-.082,1.338-.192,1.743-.914c.422-.714.635-1.965.635-4.143v-.007C14.966,82.955,14.752,81.713,14.329,80.991ZM5.612,87.95V82.338l4.677,2.806Z" transform="translate(0 -80)"/>
                          </g>
                        </g>
                        <rect width="18" height="18" transform="translate(733.846 5629.846)" fill="none"/>
                      </g>
                    </g>
                  </svg>
                </a>
              </li>
              <li class="footer-bottom__soc-item">
                <a class="footer-bottom__soc-link social-link" href="#" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                    <g transform="translate(-844 -5626)">
                      <g class="soc-ellipse" transform="translate(844 5626)">
                        <circle cx="18" cy="18" r="18" stroke="none"/>
                        <circle cx="18" cy="18" r="17.5"/>
                      </g>
                      <g class="inner-icon" transform="translate(56.626 4.626)">
                        <g transform="translate(798.899 5631.46)">
                          <path data-name="Path 64" d="M54.807,0C50.366,0,48,2.846,48,5.949a4.346,4.346,0,0,0,2.091,3.8c.2.088.3.051.345-.131.038-.138.208-.8.29-1.114a.286.286,0,0,0-.069-.282,3.69,3.69,0,0,1-.767-2.239,4.365,4.365,0,0,1,4.649-4.266,3.98,3.98,0,0,1,4.3,4c0,2.659-1.407,4.5-3.235,4.5A1.448,1.448,0,0,1,54.086,8.44a19.255,19.255,0,0,0,.855-3.271,1.257,1.257,0,0,0-1.3-1.382,2.145,2.145,0,0,0-1.866,2.389,3.406,3.406,0,0,0,.308,1.457s-1.019,4.116-1.208,4.885a10.931,10.931,0,0,0,.075,3.59.107.107,0,0,0,.194.05A12.761,12.761,0,0,0,52.821,13c.126-.463.641-2.34.641-2.34a2.8,2.8,0,0,0,2.362,1.126c3.1,0,5.346-2.728,5.346-6.112C61.158,2.428,58.381,0,54.807,0Z" transform="translate(-48.004)"/>
                        </g>
                        <rect width="18" height="18" transform="translate(796.374 5630.374)" fill="none"/>
                      </g>
                    </g>
                  </svg>
                              
                </a>
              </li>
              <li class="footer-bottom__soc-item">
                <a class="footer-bottom__soc-link social-link" href="#" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                    <g transform="translate(-902 -5626)">
                      <g class="soc-ellipse" transform="translate(902 5626)">
                        <circle cx="18" cy="18" r="18" stroke="none"/>
                        <circle cx="18" cy="18" r="17.5"/>
                      </g>
                      <g class="inner-icon" transform="translate(4.636 1.636)">
                        <path d="M13.113,3.826,14.25,2.738V2.5H10.311L7.5,9.487,4.31,2.5H.18v.238l1.328,1.6a.557.557,0,0,1,.18.465v6.284a.716.716,0,0,1-.192.623L0,13.522v.235H4.242v-.238l-1.5-1.81a.739.739,0,0,1-.206-.623V5.65l3.724,8.11H6.7L9.9,5.65v6.461c0,.17,0,.205-.112.317L8.635,13.542v.239h5.588v-.238l-1.11-1.087a.33.33,0,0,1-.127-.317v-8a.329.329,0,0,1,.126-.317Z" transform="translate(908.26 5634.052)"/>
                        <rect width="18" height="18" transform="translate(906.364 5633.364)" fill="none"/>
                      </g>
                    </g>
                  </svg>
                </a>
              </li>
              <li class="footer-bottom__soc-item">
                <a class="footer-bottom__soc-link social-link" href="#" target="_blank">
                  <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                    <g transform="translate(-786 -5626)">
                      <g class="soc-ellipse" transform="translate(786 5626)">
                        <circle cx="18" cy="18" r="18" stroke="none"/>
                        <circle cx="18" cy="18" r="17.5"/>
                      </g>
                      <g class="inner-icon" transform="translate(-54.781 2.219)">
                        <g transform="translate(851.341 5635.702)">
                          <g transform="translate(0 0)">
                            <path d="M8.611,36.066c1.709-1.026,2.63.42,1.754,2.055s-1.684,2.712-2.1,2.712-.734-1.1-1.215-3.013c-.492-1.98-.492-5.55-2.552-5.145C2.55,33.06,0,36.108,0,36.108l.606.8s1.257-.991,1.675-.5,2.029,6.478,2.56,7.584c.463.969,1.749,2.247,3.154,1.336s6.1-4.915,6.938-9.645S9.29,31.95,8.611,36.066Z" transform="translate(0 -32.54)"/>
                          </g>
                        </g>
                        <rect width="18" height="18" transform="translate(849.781 5632.781)" fill="none"/>
                      </g>
                    </g>
                  </svg>
                </a>
              </li>
            </ul>
            <div class="footer-bottom__soc-expand">
              <button class="footer-bottom__soc-expbtn" type="button">
                <svg xmlns="http://www.w3.org/2000/svg" width="36" height="36" viewBox="0 0 36 36">
                  <g transform="translate(-831 -8512)">
                    <g class="soc-ellipse" transform="translate(831 8512)">
                      <circle cx="18" cy="18" r="18" stroke="none"/>
                      <circle cx="18" cy="18" r="17.5"/>
                    </g>
                    <g transform="translate(-2 -2)">
                      <rect class="wt" width="16" height="2" transform="translate(843 8531)"/>
                      <rect class="vertical wt" width="16" height="2" transform="translate(852 8524) rotate(90)"/>
                    </g>
                  </g>
                </svg>
              </button>
            </div>
          </div>