window.addEventListener('DOMContentLoaded', () => {

  function remoceActives(nodeList) {
    nodeList.forEach(nodeItem => nodeItem.classList.remove('active'));
  }
  function setActiveName() {
    const activeSlideText = document.querySelector('.slick-active .title').innerText;
    const heroBottomNames = document.querySelectorAll('.main__hero-name');
    if(heroBottomNames) {
      heroBottomNames.forEach(name => {
        if (name.innerHTML === activeSlideText) {
          remoceActives(heroBottomNames);
          name.classList.add('active');
        }
      });
    }    
  }
  const mainSlider = document.querySelector('.main__hero');
  if(mainSlider) {
    $('.main__hero').slick({
      arrows: false,
      dots: true,
    }).on('beforeChange', () => {
      setTimeout(() => {
        setActiveName();
      }, 200);
    });  
    setActiveName();
  }
  
  const donateCustomInput = document.querySelector('.main__donate-custom input');  
  if(donateCustomInput) {
    donateCustomInput.addEventListener('input', () => {
      const checkedRadio = document.querySelector('.main__donate-radio:checked');
      if(checkedRadio) {
        checkedRadio.checked = false;
      }
    });
  }
  function expandSocials(btnClass, elementClass) {
    const socialExpandBtn = document.querySelector(btnClass);
    const socials = document.querySelector(elementClass);
    if(socialExpandBtn) {
      socialExpandBtn.addEventListener('click', evt => {
        evt.preventDefault();
        socials.classList.toggle('active');
        socialExpandBtn.classList.toggle('active');
      });
    }
  }
  expandSocials('.footer-bottom__soc-expbtn', '.footer-bottom__socgroup');
  expandSocials('.blog-item__socials-expbtn', '.blog-item__socials-group');
  

  const searchBtn = document.querySelector('.header__search-btn');
  const searchInput = document.querySelector('.header__search-input');

  searchBtn.addEventListener('click', evt => {
    evt.preventDefault();
    searchInput.classList.toggle('active');
  });


  const heroNames = document.querySelectorAll('.main__hero-name');
  const dotss = document.querySelectorAll('.main__hero .slick-dots button');

  if(heroNames) {
    heroNames.forEach((item, index) => {
      dotss[index].innerText = item.innerText;
    });
  }

  const resObs = new ResizeObserver(enableSlick);
  resObs.observe(document.body);

  function enableSlick(entries) {
    if(entries[0].contentRect.width >= 992) {
      
      $(".main__stories-items").not('.slick-initialized').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        centerMode: false,
        autoplay: true,
        autoplaySpeed: 6000
      });
    } else {
      $(".main__stories-items.slick-initialized").slick('unslick');
    }
    
  }
  const fakePrev = document.querySelector('.main__stories-prev');
  const fakeNext = document.querySelector('.main__stories-next');

  function addDisable(original) {
    const fakeNext = document.querySelector('.main__stories-next');
    const fakePrev = document.querySelector('.main__stories-prev');
    if(original.classList.contains('slick-disabled')) {
      if (original.classList.contains('slick-next')) {
        fakeNext.classList.add('disable');
      }
      if (original.classList.contains('slick-prev')) {
        fakePrev.classList.add('disable');
      }
    }
    else {
      fakeNext.classList.remove('disable');
      fakePrev.classList.remove('disable');
    }
  }
  
  if(fakeNext) {
    fakeNext.addEventListener('click', () =>{
      const nextSlide = document.querySelector('.main__stories-items .slick-next');
      addDisable(nextSlide);
  
      if(nextSlide) {
        nextSlide.click();
      }
    });
  }
  if(fakePrev) {
    fakePrev.addEventListener('click', () =>{
      const prevSlide = document.querySelector('.main__stories-items .slick-prev');
      addDisable(prevSlide);
      
      if(prevSlide) {
        prevSlide.click();
      }
    });
  }

  const toTop = document.querySelector('.footer-bottom__totop');
  const position = $('#main').offset().top;
  toTop.addEventListener('click', e => {
    e.preventDefault();
    $([document.documentElement, document.body]).animate({
        scrollTop: position
    }, 500);
  });

  const observerTrigger = document.querySelector('.footer');
  const main = document.getElementById('main');
  
  let observer = new IntersectionObserver(entries => {
    
    if(main.offsetHeight > (2 * window.innerHeight)) {
      if(entries[0].boundingClientRect.top < toTop.getBoundingClientRect().bottom) {
        toTop.classList.add('visible');
      } else {
        toTop.classList.remove('visible');
      }
    }
    
  }, {threshold: .1});
  observer.observe(observerTrigger);  
  
});