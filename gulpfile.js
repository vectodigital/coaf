const { src, dest, parallel, watch } = require("gulp"),
  sass = require("gulp-sass"),
  browserSync = require("browser-sync").create(),
  uglify = require("gulp-uglify-es").default,
  concat = require("gulp-concat"),
  autoprefixer = require("gulp-autoprefixer"),
  cleanCss = require("gulp-clean-css"),
  rename = require("gulp-rename");

  const name = "coaf"

function sync() {
  browserSync.init({
    proxy: name,
    online: true,
    notify: false,
  });
}
function scss() {
  return src("gulp/scss/**/*.scss")
    .pipe(sass())
    .pipe(
      autoprefixer({ overrideBrowserslist: ["last 4 versions"], grid: true })
    )
    .pipe(rename({ suffix: ".min" }))
    .pipe(
      cleanCss({
        level: { 1: { specialComments: 0 } }
      })
    )
    .pipe(dest(name + "/css/"))
    .pipe(browserSync.stream());
}
function mainJs() {
  return src("gulp/js/main.js")
    .pipe(uglify())
    .pipe(rename({ suffix: ".min" }))
    .pipe(dest(name + "/js"))
    .pipe(browserSync.stream());
}

function startWatch() {
  watch("gulp/scss/*.scss", scss);
  watch(name + "/index.html").on("change", browserSync.reload);
  watch("gulp/js/main.js", mainJs);
}

exports.sync = sync;
exports.scss = scss;
exports.mainJs = mainJs;

exports.default = parallel( scss, mainJs, sync, startWatch);
